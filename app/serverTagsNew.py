# RedisEdge realtime video analytics web server
import argparse
import cv2
import io
import numpy as np
import redis
from urllib.parse import urlparse
from PIL import Image, ImageDraw, ImageFont
from flask import Flask, render_template, Response
import funzioni
import logging
from time import time
import json
from collections import OrderedDict
from funzioni import loadConfiguration
import os


class RedisImageStream(object):
    def __init__(self, conn, args):
        self.conn = conn
        self.camera =  'area:' + initialParameters['DATABASE']['AREA_NUMBER'] + ':camera:' + initialParameters['DATABASE']['CAMERA_NUMBER']
        self.draw = self.camera + ':draw'
        self.field = args.field.encode('utf-8') 
        self.authDict =  'area:0:idVsAuth'

        self.TAG_TRANSMISSION_DELAY_MSEC = initialParameters['DRAWING_WITH_TAG']['TAG_TRANSMISSION_DELAY_MSEC'] 
        self.ARBITRARY_DELAY = initialParameters['DRAWING_WITH_TAG']['ARBITRARY_DELAY']


        fontPath = './font/FreeMonoBold.ttf'
        self.idFontSize = 10
        self.idFont = ImageFont.truetype(fontPath,self.idFontSize)

        tagFontSize = 10
        self.tagFont = ImageFont.truetype(fontPath,tagFontSize)

        self.coloreTagNonAssociato = 'white'
        self.coloreTagAssociato = 'white'

        self.counterMax = 50
        self.greenCounter = OrderedDict()
        self.redCounter = OrderedDict()

        # codici eventi
        self.greenIn = initialParameters['EVENTS']['GREEN_IN']
        self.greenOut = initialParameters['EVENTS']['GREEN_OUT']
        self.redIn = initialParameters['EVENTS']['RED_IN']
        self.redOut = initialParameters['EVENTS']['RED_OUT']

        self.pointsGreen = [(initialParameters['EVENTS']['GREEN_LINE_X1'],initialParameters['EVENTS']['GREEN_LINE_Y1']) ,(initialParameters['EVENTS']['GREEN_LINE_X2'],initialParameters['EVENTS']['GREEN_LINE_Y2'])]
        self.pointsRed = [(initialParameters['EVENTS']['RED_LINE_X1'],initialParameters['EVENTS']['RED_LINE_Y1']) ,(initialParameters['EVENTS']['RED_LINE_X2'],initialParameters['EVENTS']['RED_LINE_Y2'])]
    
        self.frame_shape = np.array((initialParameters['FRAME']['FRAME_WIDTH'],initialParameters['FRAME']['FRAME_HEIGHT']))

        self.badgeReader = {}
        self.color = {}
        for reader in initialParameters['BADGE_READER'].items():
            _id = reader[1]['ID']
            self.badgeReader[_id] = (reader[1]['X']*initialParameters['FRAME']['FRAME_WIDTH'], reader[1]['Y']*initialParameters['FRAME']['FRAME_HEIGHT'])
            self.color[_id] = tuple(np.random.choice(range(256), size=3)) 

    def greenCounterDecrease(self, id):
        self.greenCounter[id] -= 1
        if self.greenCounter[id] <= 0:
            del self.greenCounter[id]
            return True
        else:
            return False    
    
    def redCounterDecrease(self, id):
        self.redCounter[id] -= 1
        if self.redCounter[id] == 0:
            del self.redCounter[id]
            True
        else:
            False


    def get_last(self):
        ''' Gets latest from camera and model '''

        t = int(time()*1e3) - self.TAG_TRANSMISSION_DELAY_MSEC

        cmsg = self.conn.xrange(self.camera, min=t, max='+', count=1)

        if cmsg:

            last_id = cmsg[0][0].decode('utf-8')
            label = f'{self.camera}:{last_id}'
            data = io.BytesIO(cmsg[0][1][self.field])
            img = Image.open(data)
            
            t_det = int(last_id[:-2]) + self.TAG_TRANSMISSION_DELAY_MSEC - self.ARBITRARY_DELAY
            dmsg = self.conn.xrange(self.draw,min=t_det , count=1)

                    
            draw = ImageDraw.Draw(img, "RGBA")             

            # draw badge reader

            
            for id, coords in self.badgeReader.items():   
                badge_r = 20
                (bX,bY) = coords
                leftUpPoint = (bX-badge_r, bY-badge_r)
                rightDownPoint = (bX+badge_r, bY+badge_r)
                twoPointListB = [leftUpPoint, rightDownPoint]
                draw.ellipse(twoPointListB,outline=self.color[id],width=2)               
                badge_r = 2
                leftUpPoint = (bX-badge_r, bY-badge_r)
                rightDownPoint = (bX+badge_r, bY+badge_r)
                twoPointListB = [leftUpPoint, rightDownPoint]
                draw.ellipse(twoPointListB,outline=self.color[id],width=4)   

                # Draw areas rectangle
                boxCoordinates = ((400,230),(500,330))
                draw.rectangle(boxCoordinates, width=1, outline='blue') # box

            #events = []
            #IDs = []

            if dmsg:

                #print(dmsg[0][1])
                msg = dmsg[0][1]

                area_id = msg[b'area_id']
                camera_id = msg[b'camera_id']
                frame_id = msg[ b'frame_id']
                targets = json.loads(msg[b'targets'])
                tags = eval(msg[b'tags'])
                #print(tags)

                # draw tags

                tagN = int(tags[b'tagN'])
                #print(type(tags['tagPos']))
                tagPos = tags[b'tagPos']
                tagID = tags[b'tagID']
                for tagId in range(tagN):                             
                    tagX = tagPos[2*tagId]
                    tagY = tagPos[2*tagId+1]

                    r = 5
                    leftUpPoint = (tagX-r, tagY-r)
                    rightDownPoint = (tagX+r, tagY+r)
                    tagCoordinates = [ leftUpPoint, rightDownPoint ]
                    #print(twoPointList)

                    draw.ellipse(tagCoordinates, outline=(self.coloreTagNonAssociato))

                # gestione grassetti linee    
                IDs = []
                #print(IDs)

                ###
                ###
                ###
                for idx,target in enumerate(targets):     
                    #print(target)  
                    id = target['target_id']
                    IDs += [id]
                    events = eval(target['zone_events'])

                    if self.greenIn in events or self.greenOut in events:
                        self.greenCounter[id] = self.counterMax
                        width = 4
                    elif id in self.greenCounter and self.greenCounter[id] > 0:
                        if self.greenCounterDecrease(id):
                            width = 1
                        else:
                            width = 4

                    elif self.redIn in events or self.redOut in events:
                        self.redCounter[id] = self.counterMax
                        width = 4
                    elif id in self.redCounter and self.redCounter[id] > 0:
                        if self.redCounterDecrease(id):
                            width = 1
                        else:
                            width = 4
                    else:
                        width = 1
    

                    position = target['screen_box']
                    x1 = position[0] * self.frame_shape[0]
                    y1 = position[1] * self.frame_shape[1]
                    x2 = position[2] * self.frame_shape[0]
                    y2 = position[3] * self.frame_shape[1]

                    # Centroid
                    cX, cY = funzioni.centroid(x1,y1,x2,y2)
                    r = 3
                    leftUpPoint = (cX-r, cY-r)
                    rightDownPoint = (cX+r, cY+r)
                    twoPointListC = [leftUpPoint, rightDownPoint]

                    # Feet
                    feet = np.array(target['screen_feet']) * self.frame_shape
                    feetCoordinatesX = float(feet[0])
                    feetCoordinatesY = float(feet[1])

                    # ID
                    #draw.text((x1 + 5, y1 + 5), str(IDs[box]), font=ImageFont.truetype(font,fontSize), fill=(255,0,0,255))
                    
                    r = 3
                    leftUpPoint = (feetCoordinatesX-r, feetCoordinatesY-r)
                    rightDownPoint = (feetCoordinatesX+r, feetCoordinatesY+r)
                    twoPointList = [ leftUpPoint, rightDownPoint ]
                    #print(twoPointList)


                    
                    colorText = (255,255,255,255)
                    # Tags
                    diz = self.conn.hget(self.authDict,id)
                    if diz:
                        auth_code = json.loads(diz)['auth']                    
                        #print(auth_code)
                        if auth_code == '0':
                            outline = 'green'
                            color = (0,128,0,255)
                        elif auth_code == '1':
                            outline = 'cyan'
                            color = (0,0,255,255)
                        elif auth_code == '2':
                            outline = 'yellow'
                            color = (255,255,0,255)
                        elif auth_code == '-1':
                            outline = 'black'
                            color = (0,0,0,255)
                        else:
                            color = (0,0,0,255)
                            outline = 'black'

                    else:
                        color = (0,0,0,255)
                        outline = 'black'

                    #print(events)
                    if 100 in events and auth_code != '0': 
                        #print('entrati')
                        boxCoordinates = ((400,230),(500,330))
                        draw.rectangle(boxCoordinates, width=3, fill=(176,224,230, 75), outline='blue') # box
                    ########################## da fare ancora #############################
                    
                    tag_id = target['tag_id']
                    
                    if tag_id != 'null':
                        
                        (tagX, tagY) = target['ground_position']                        

                        draw.line([(cX,cY),(tagX,tagY)],fill=self.coloreTagAssociato)

                        r = 4
                        leftUpPoint = (tagX-r, tagY-r)
                        rightDownPoint = (tagX+r, tagY+r)
                        tagCoordinates = [ leftUpPoint, rightDownPoint ]
                        draw.ellipse(tagCoordinates, fill=color)

                        tagString = 'T:{0}'.format(tag_id)
                        text_size = self.idFont.getsize(tagString)
                        draw.rectangle(((x1+5, y2 - text_size[1]/2),(x1+5+text_size[0],y2  +text_size[1]/2)), width=width, outline=outline,fill=color) # rettangolo id
                        draw.text((x1 + 5, y2 -self.idFontSize/2), tagString, font=self.tagFont,fill=self.coloreTagAssociato)


                                                
                    draw.rectangle(((x1, y1), (x2, y2)), width=width, outline=color) # box
                    draw.ellipse(twoPointListC, fill=color) # box centroid
                    draw.ellipse(twoPointList, fill=('cyan')) # box feet

                    
                    idString = 'ID:{0:05d}'.format(int(id))
                    text_size = self.idFont.getsize(idString)
                    draw.rectangle(((x1+5, y1 -text_size[1]/2),(x1+5+text_size[0],y1  +text_size[1]/2)), width=width, outline=outline,fill=color) # rettangolo id
                    draw.text((x1 + 5, y1 -self.idFontSize/2), idString, font=self.idFont, fill=colorText,outline=outline) # box id

                    #print(twoPointList)
                    pass

                for id in list(self.greenCounter):
                    if id not in IDs:
                        self.greenCounterDecrease(id)

                for id in list(self.redCounter):
                    if id not in IDs:
                        self.redCounterDecrease(id)

                
            else:
                
                for id in list(self.greenCounter):
                    self.greenCounterDecrease(id)
                                           

                for id in list(self.redCounter):
                    self.redCounterDecrease(id)
                
            # set the width of green and red lines 
            greenWidth = 1
            for _,value in self.greenCounter.items():
                if value > 0:
                    greenWidth = 4
                    break

            redWidth = 1
            for _,value in self.redCounter.items():
                if value > 0:
                    redWidth = 4
                    break
            
            draw.line(self.pointsRed,fill='red', width=redWidth)
            draw.line(self.pointsGreen,fill='green',width=greenWidth)

                        
            arr = np.array(img)
            arr = cv2.cvtColor(arr, cv2.COLOR_BGR2RGB)
            cv2.putText(arr, label, (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,255), 1, cv2.LINE_AA)
            ret, img = cv2.imencode('.jpg', arr)
            return img.tobytes()
        else:
            # TODO: put an 'we're experiencing technical difficlties' image
            blank_image = np.zeros((initialParameters['FRAME']['FRAME_HEIGHT'],initialParameters['FRAME']['FRAME_WIDTH'],3), np.uint8)
            return blank_image.tobytes()
        #p.xrevrange(self.camera, count=1)  # Latest frame


def gen(stream):
    while True:
        frame = stream.get_last()
        yield (b'--frame\r\n'
               b'Pragma-directive: no-cache\r\n'
               b'Cache-directive: no-cache\r\n'
               b'Cache-control: no-cache\r\n'
               b'Pragma: no-cache\r\n'
               b'Expires: 0\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

conn = None
args = None
app = Flask(__name__)

@app.route('/video')
def video_feed():
    return Response(gen(RedisImageStream(conn, args)),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    initialParameters = loadConfiguration()
    parser = argparse.ArgumentParser()
    parser.add_argument('--field', help='Image field name', type=str, default='image')
    parser.add_argument('--fmt', help='Frame storage format', type=str, default='.jpg')
    parser.add_argument('-u', '--url', help='Redis URL', type=str, default='redis://127.0.0.1:6379')
    args = parser.parse_args()



    # Set up Redis connection
    if os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', False):
        url = urlparse(args.url)    
        #url = urlparse(initialParameters['DBURL'])
    else:
        url = urlparse('redis://' + initialParameters['DATABASE']['HOST'] + ':' + initialParameters['DATABASE']['PORT'])
    conn = redis.Redis(host=url.hostname, port=url.port)
    if not conn.ping():
        raise Exception('Redis unavailable')
    
    app.run(host='0.0.0.0',port=initialParameters['DRAWING_WITH_TAG']['SERVER_OUTPUT_PORT'])
