import redis
from urllib.parse import urlparse

class redis_manager():
    '''
    Set up Redis connection

    Usage:
        - db = redis_manager().connect(host_name,port)
    '''

    def __init__(self):

        shared_state = {}
        self.__dict__ = shared_state

    def connect(self, host, port):
        '''
        Establish Database Connection

        Arguments:
            - host: Host name for the connection.
            - port: Connection port.
        
        Returns:
            - connection: Redis client (<class 'redis.client.Redis'>)

        Note:
            - exit if loading fails
        '''
        
        connectionPool = redis.ConnectionPool(host=host, port=port, db=0)

        connection = redis.StrictRedis(connection_pool=connectionPool)
        
        if not connection.ping():
            raise Exception('Redis unavailable')
        else:
            #print('Redis connection OK')
            return connection

if __name__ == '__main__':

    help(redis_manager)