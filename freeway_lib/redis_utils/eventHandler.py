from redisManager import redis_manager

class eventHandler(redis_manager):

    def __init__(self, streamOutput, host, port, camera) -> None:
        '''
        Class which manage the relevant informations about an occurrent event

        Arguments:
            - streamOutput: name of the output Redis stream.
            - host: Host name for the connection.
            - port: Connection port.
            - camera: name/id of the camera.
        '''

        self.streamOutput = streamOutput
        self.camera = camera
        self.db = redis_manager().connect(host,port)

    def registerEvent(self, eventCode, eventTS, tag=None, auth=-1):

        '''
        Register the relevant informations about an occurrent event

        Arguments:
            - eventCode: Code of the occurred event.
            - eventTS: Timestamp in milliseconds at which the event was registered.
            - tag: (optional, default 'None') Tag of the bluethooth device/badge if pairing occurred.
            - auth: (optional, default '-1') Authorization code if pairing occurred.
        
        Returns:
            - None
        '''
        
        event = {
            'camera': self.camera,
            'event_code': eventCode,
            'event_ts': eventTS,
            'tag': str(tag),
            'authorization_code': str(auth)
        }
        
        self.db.xadd(self.streamOutput,event,maxlen=1000)
        

if __name__ == '__main__':


    '''
    streamOutput = 'event:Queue'
    host = '127.0.0.1'
    port = '6379'
    camera = '101'

    eh = eventHandler(streamOutput,host,port, camera)
    eh.registerEvent(0,0)
    '''

    pass