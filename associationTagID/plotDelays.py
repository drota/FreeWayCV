from matplotlib import pyplot as plt
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--service', help='which plot', type=str, default='coupling')
parser.add_argument('-d', '--delay', help='fixed delay', type=str, default='750')
args = parser.parse_args()



if args.service == 'pairing':
    filename = "delays.txt"
    
elif args.service == 'coupling':
    filename = "differenze_temporali_tag_det.log"

input1 = open(filename, "r").read().strip()
l = input1.split("\n")

if args.service == 'pairing':
    delays = np.array([ int(x) for x in l ])
    bins = np.arange(delays.min(), delays.max()+1)-0.5
    
elif args.service == 'coupling':
    fixed_delay = args.delay
    #delays750 = np.array([ float(x.split("\t")[1]) for x in l if x.split("\t")[0] == '750' ])
    delays = np.array([ float(x.split("\t")[2]) for x in l if x.split("\t")[0] == fixed_delay and abs(float(x.split("\t")[2]))<500])
    bins = np.arange(-400,400)

print(delays)
#print(l)

#plt.hist(delays, bins=np.arange(delays.min(), delays.max()+1)-0.5)
print('mean: {}'.format(delays.mean()))
print('stdev: {}'.format(delays.std()))
plt.hist(delays, bins=bins)
#plt.hist(delays750, bins=bins)
plt.gca().set(title='Delay (ms)', ylabel='Frequency')
#plt.plot(delays)
plt.show()