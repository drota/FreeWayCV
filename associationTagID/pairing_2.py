import redis
from time import time
import numpy as np
from scipy.spatial import distance as dist
from scipy.optimize import linear_sum_assignment
from collections import OrderedDict
from funzioni import showExceptionInfo, loadConfiguration, evaluateLineParameters,isUnder, clearOrderedDict, Box,Line, Area, compute_pairs
import argparse
from urllib.parse import urlparse
import os
import sys
import logging
import logging.config
import yaml
import io
from PIL import Image
from redisManager import redis_manager
from eventHandler import eventHandler
import json

class pairing:
    """object that pairs people detection and beacon tags"""

    def __init__(self, url):

        with open('./pair_log_config.yaml', 'r') as f:
            config = yaml.safe_load(f.read())
            logging.config.dictConfig(config)

        self.logger = logging.getLogger('pair_logger_debug')
        self.logger.setLevel('INFO')
        self.logger.debug('New Execution')

        self.connection = redis_manager()
        self.db = self.connection.connect(url.hostname, url.port)

        self.streamFrame = 'area:' + initialParameters['DATABASE']['AREA_NUMBER'] + ':camera:' + initialParameters['DATABASE']['CAMERA_NUMBER']
        self.streamTagDetections = self.streamFrame + ':tagDet'
        streamEvents = 'area:' + initialParameters['DATABASE']['AREA_NUMBER'] + ':events'
        self.lastID = '$'
        self.backupFeetPos = OrderedDict()

        self.eventHandler = eventHandler(streamEvents,url.hostname,url.port, initialParameters['DATABASE']['CAMERA_NUMBER'])

        # equazioni delle linee per la registrazione degli eventi
        frame_width = initialParameters['FRAME']['FRAME_WIDTH']
        frame_height = initialParameters['FRAME']['FRAME_HEIGHT']
        pointsGreen = [(initialParameters['EVENTS']['GREEN_LINE_X1'],initialParameters['EVENTS']['GREEN_LINE_Y1']) ,(initialParameters['EVENTS']['GREEN_LINE_X2'],initialParameters['EVENTS']['GREEN_LINE_Y2'])]
        pointsRed = [(initialParameters['EVENTS']['RED_LINE_X1'],initialParameters['EVENTS']['RED_LINE_Y1']) ,(initialParameters['EVENTS']['RED_LINE_X2'],initialParameters['EVENTS']['RED_LINE_Y2'])]
        self.greenParam = evaluateLineParameters(pointsGreen)
        self.redParam = evaluateLineParameters(pointsRed)
        
        # current frame
        self.upperBound = np.sqrt(pow(frame_width,2) + pow(frame_height,2)) + 1 # lim sup per la distanza
        self.dMax = self.upperBound * initialParameters['PAIRING_DETECTION_TAG']['MAX_PERCENTAGE_DIST'] # distanza massima oltre la quale non considero il tag associato a nessuna detection

        # dictionary
        self.people = OrderedDict() # dizionario che associa le persone all' id del tag
        self.disappeared = OrderedDict()
        self.authCode = OrderedDict()
        self.eventsHistory = OrderedDict() # registro degli eventi per un dato id
        self.maxDisappeared = initialParameters['PAIRING_DETECTION_TAG']['MAX_DISAPPEARED']

        # codici eventi
        self.greenIn = initialParameters['EVENTS']['GREEN_IN']
        self.greenOut = initialParameters['EVENTS']['GREEN_OUT']
        self.redIn = initialParameters['EVENTS']['RED_IN']
        self.redOut = initialParameters['EVENTS']['RED_OUT']

        # Load lines
        self.lines = []
        for data in initialParameters['EVENTS']['LINES'].items():
            line = data[1]
            self.lines.append( Line(    line['ID'],
                                        line['COLOR'],
                                        [line['X1']*frame_width,line['Y1']*frame_height, line['X2']*frame_width,line['Y2']*frame_height],
                                        line['A_TO_B_CODE'],
                                        line['B_TO_A_CODE'],
                                        initialParameters['DATABASE']['AREA_NUMBER'],
                                        initialParameters['DATABASE']['CAMERA_NUMBER']
                                )
                            )

        # Load Boxes
        self.boxes = []
        for data in initialParameters['EVENTS']['BOXES'].items():
            box = data[1]
            #print(box)
            self.boxes.append( 
                Box( 
                    box['ID'],
                    box['COLOR'],
                    [box['X1']*frame_width, box['Y1']*frame_height, box['X2']*frame_width, box['Y2']*frame_height],
                    box['CROSS_IN_CODE'],
                    box['CROSS_OUT_CODE'],
                    box['AUTH_REQUESTED'],
                    box['IS_IN_CODE'],
                    initialParameters['DATABASE']['AREA_NUMBER'],
                    initialParameters['DATABASE']['CAMERA_NUMBER']
                )
            )
                            
        # Load areas
        self.areas = []
        for data in initialParameters['EVENTS']['AREAS'].items():
            area = data[1]
            coordinates = [ (x[0] * frame_width ,x[1] * frame_height) for x in area['COORDINATES'] ]
            self.areas.append( 
                Area(
                    area['ID'],
                    area['COLOR'],
                    coordinates,
                    area['CROSS_IN_CODE'],
                    area['CROSS_OUT_CODE'],
                    area['AUTH_REQUESTED'],
                    area['IS_IN_CODE'],
                    initialParameters['DATABASE']['AREA_NUMBER'],
                    initialParameters['DATABASE']['CAMERA_NUMBER']
                )
            )


        self.streamDraw = self.streamFrame + ':draw'

    def __iter__(self):
        return self

    def __next__(self):
        
        try:
            combinedDetection = self.db.xread({self.streamTagDetections:self.lastID},count=1,block=0)

            if combinedDetection:
                
                newPairs,newPairsAuth = self.pairInFrame(combinedDetection)
                self.updateDict(newPairs,newPairsAuth)

                pairsID = []
                pairsTag = []
                authCode = []
                events = OrderedDict()

                for key,value in self.people.items():
                    pairsID += [key]
                    pairsTag += [value]
                    authCode += [self.authCode[key]]           

                    # update dict for feet in rt
                    if not self.connection.isBadge(key):
                        self.connection.updateAuthDict(key,self.authCode[key], False)
                        
                
                detections = eval(combinedDetection[0][1][0][1][b'detections'])
                if detections:
                    
                    detectionsIDs = eval(detections[b'IDs'])
                    frameTS = eval(detections[b'ref'])
                    detectionsFeet = eval(detections[b'feet'])
                    feetPos = OrderedDict()
                    for idx,id in enumerate(detectionsIDs):
                        feetPos[id] = [detectionsFeet[2*idx],detectionsFeet[2*idx+1]]
                        events[id] = []   

                        # check if an event occurs
                        #if id in self.backupFeetPos:
                        if id not in self.eventsHistory:
                            self.eventsHistory[id] = []

                        # EVENTI LINEE              
                        for line in self.lines:

                            event = line.crossLine(id,feetPos[id])
                            if event != 0:
                                events[id] += [event]
                                self.eventsHistory[id].append(event)

                                args = [event,frameTS]
                                if id in self.people:
                                    args += [self.people[id], self.authCode[id]]                              
                                self.eventHandler.registerEvent(*args)

                        # EVENTI BOX
                        for box in self.boxes:

                            if id in self.authCode:
                                auth_code = str(self.authCode[id])
                            else:
                                auth_code = '-1'

                            event = box.crossBoxBorders(id, feetPos[id], auth_code)
                            #print(event)
                            if event != 0:
                                events[id] += [event]
                                self.eventsHistory[id].append(event)

                                args = [event,frameTS]
                                if id in self.people:
                                    #print('{}\t{}'.format(auth_code,type(auth_code)))
                                    args += [self.people[id], auth_code]                              
                                self.eventHandler.registerEvent(*args)


                        # EVENTI AREE
                        for area in self.areas:

                            if id in self.authCode:
                                auth_code = str(self.authCode[id])
                            else:
                                auth_code = '-1'

                            event = area.crossAreaBorders(id, feetPos[id], auth_code)
                            #print(event)
                            if event != 0:
                                
                                events[id] += [event]
                                self.eventsHistory[id].append(event)

                                args = [event,frameTS]
                                if id in self.people:
                                    #print('{}\t{}'.format(auth_code,type(auth_code)))
                                    args += [self.people[id], auth_code]                              
                                self.eventHandler.registerEvent(*args)


                        self.backupFeetPos[id] = feetPos[id]
                        
                        if not all(elem == 0 for elem in events[id]):
                            self.logger.debug('{} - {} - {}'.format(eval(combinedDetection[0][1][0][1][b'detections'])[b'ref'],id,events[id]))

                    maxLen = 50  # per evitare che la lunghezza di self.backupFeetPos cresca indefinitamente
                    clearOrderedDict(self.backupFeetPos,maxLen)

                    #self.logger.debug('{}\t{}\t{}'.format(detectionsIDs,feetPos,self.backupFeetPos))
                    #self.logger.debug('ok')

                    #for id,tag in zip(pairsID,pairsTag):
                    #    print('{}\t{}'.format(id,tag))
                    #print()
                
                drawInfo = {
                    b'detections' : combinedDetection[0][1][0][1][b'detections'],
                    b'tags' : combinedDetection[0][1][0][1][b'tags'],
                    b'pairsID' : str(pairsID),
                    b'pairsTag' : str(pairsTag),
                    b'authCode' : str(authCode),
                    b'events' : str(events)
                }
                
                #print(drawInfo[b'detections'])
                self.db.xadd(self.streamDraw,drawInfo,maxlen=1000) 
                    

        except Exception as e:
            self.logger.error('Exception in Pairing', exc_info=True)
            showExceptionInfo(e,'Pairing')
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            exit()

    def pairInFrame(self, combinedDetection):
        ''' 
        permette di associare l'id della CV detection a quello
        della detection delle antenne
        '''

        #self.backupID = self.lastID        
        self.lastID = combinedDetection[0][1][0][0]

        detections = eval(combinedDetection[0][1][0][1][b'detections'])
        if detections:
            detectionsNumber = int(detections[b'people'])
            detectionsIDs = eval(detections[b'IDs'])
        else:
            detectionsNumber = 0
            detectionsIDs = []

        #print(combinedDetection[0][1][0][1][b'tags'])
        tags = eval(combinedDetection[0][1][0][1][b'tags'])
        tagsNumber = int(tags[b'tagN'])
        tagIDs = tags[b'tagID']
        tagAuth = tags[b'tagAuth']

        newPairs = OrderedDict()
        newPairsAuth = OrderedDict()

        if not detections or tagsNumber == 0:
            return newPairs, newPairsAuth

        alreadyBadged = []
        feetPos = np.zeros((detectionsNumber, 2), dtype="int")
        feet = eval(detections[b'feet'])

        first_check = OrderedDict()
        for counter in range(detectionsNumber):
            feetPos[counter] = (feet[2*counter], feet[2*counter+1])

            #print(0)
            first_check[counter] = self.connection.isBadge(detectionsIDs[counter]) 
            if first_check[counter]:
                alreadyBadged += [counter]
                #print('badged')

                if detectionsIDs[counter] in self.people:
                    self.deregisterPerson(detectionsIDs[counter],True)
          
        tagPos = np.zeros((tagsNumber, 2), dtype="int")  
        for counter in range(tagsNumber):
            tagPos[counter] = (tags[b'tagPos'][2*counter], tags[b'tagPos'][2*counter+1])
        
        D = dist.cdist(feetPos,tagPos) # distances matrix      

        D[D > self.dMax] = self.upperBound + 1 # substitute large distances with upper bound
        D[alreadyBadged] = self.upperBound + 1
        
        #rows, cols = compute_pairs(D) # evaluate pairings 
        indexes = compute_pairs(D,alreadyBadged,self.upperBound + 1) # evaluate pairings 
        #print(indexes)   

        for (id_idx, tag_idx) in indexes:

            if D[id_idx][tag_idx] < (self.upperBound + 1):

                #print(1)
                if not first_check[id_idx]:#self.connection.isBadge(id_idx):
                
                    id = detectionsIDs[id_idx]
                    tag = tagIDs[tag_idx]
                    auth = tagAuth[tag_idx]
                
                    newPairs[id] = tag
                    newPairsAuth[id] = auth
                    
        return newPairs, newPairsAuth

    def registerPerson(self,id,tag,auth):
        self.people[id] = tag
        self.disappeared[id] = 0
        self.authCode[id] = auth

    def deregisterPerson(self,id, isBadge):
        if not isBadge:
            self.connection.updateAuthDict(id,'-1',False) # TODO vedo di eliminare direttamente la voce dal dizionario
        del self.people[id]
        del self.disappeared[id]
        del self.authCode[id]
        if id in self.eventsHistory:
            del self.eventsHistory[id]

    def updateDict(self, newPairs, newAuth):

        newPairsID = list(newPairs.keys()) 
        newAuthID = list(newAuth.keys())
        dictID = list(self.people.keys())

        intersection = np.intersect1d(newPairsID,dictID)
        onlyNew = np.setdiff1d(newPairsID,dictID)
        onlyDict = np.setdiff1d(dictID,newPairsID)
        
        for id in intersection:
            if self.people[id] == newPairs[id]:
                self.disappeared[id] = 0
            else:
                self.disappeared[id] += 1
                if self.disappeared[id] > self.maxDisappeared:
                    self.deregisterPerson(id,False) 
        
        for id in onlyNew:
            alreadyPresent = False
            for _,tagDict in self.people.items():
                if tagDict == newPairs[id]:
                    alreadyPresent = True
                    break
            if not alreadyPresent:
                self.registerPerson(id,newPairs[id],newAuth[id])

        for id in onlyDict:
            self.disappeared[id] += 1
            if self.disappeared[id] > self.maxDisappeared:
                self.deregisterPerson(id, False)    
        

if __name__ == '__main__':

    initialParameters = loadConfiguration()

    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--url', help='Redis URL', type=str, default='redis://127.0.0.1:6379')
    args = parser.parse_args()

    print('\nConfiguration parameters:')
    for name, value in initialParameters.items():
        print('\t{0:.<40}{1}'.format(name,value))
    print()  

    if os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', False):
        url = urlparse(args.url)    
    else:
        url = urlparse('redis://' + initialParameters['DATABASE']['HOST'] + ':' + initialParameters['DATABASE']['PORT'])

    message = pairing(url)
    
    #iterations = 0
    #time_tot = 0
    #time0 = time()
    while True:
        

        #iterations += 1
        #time_tot += (time() - time0)
        #if iterations%15==0:
        #    print(iterations/time_tot)
        #time0 = time()
        message.__next__()