from redisManager import redis_manager
import funzioni as f
import io
from PIL import Image
from time import time
from datetime import datetime
import logging
import logging.config
import yaml
import os
import cv2
import numpy as np

init = f.loadConfiguration()

class event_saver():
    def __init__(self, host, port, stream_events, dir_name):
        self.db = redis_manager().connect(host,port)
        self.stream_events = stream_events
        self.last_event_id = '$'
        self.area = init['DATABASE']['AREA_NUMBER']

        with open('./event_log_config.yaml', 'r') as f:
            config = yaml.safe_load(f.read())
            logging.config.dictConfig(config)
        self.logger = logging.getLogger('pair_logger_debug')

        self.dir_name = dir_name
        if not os.path.isdir(dir_name):
            os.makedirs(dir_name)
        
        
    def xread_event(self, record_duration):

        #emsg = self.db.xrevrange(self.stream_events,count=1)[0]
        emsg = self.db.xread({self.stream_events:self.last_event_id},count=1,block=0)[0][1][0]
        
        self.last_event_id = emsg[0].decode('utf-8')
        
        stream_camera = 'area:' + self.area + ':camera:' + emsg[1][b'camera'].decode('utf-8')

        event_ts = int(emsg[1][b'event_ts'].decode('utf-8'))
        
        startingStreamID = event_ts - record_duration
        frames = self.db.xrange(stream_camera,min=startingStreamID,max=event_ts)

        self.logger.debug('{} - {} - {} - {} - {}'.format(record_duration, event_ts, startingStreamID, stream_camera,len(frames)))

        if len(frames) > 0:

            formatted_datetime = datetime.fromtimestamp(event_ts/1e3).strftime("%d%b%Y_%H-%M-%S")
            name_prefix = self.dir_name + 'area' + self.area + '_camera' + emsg[1][b'camera'].decode('utf-8') + '_' + formatted_datetime
            
            self._save_img(frames[-1],name_prefix)

            if len(frames) > 1:
                self._save_gif(frames, name_prefix)
                self._save_video(frames, name_prefix)
                # save video

    def _process_img(self,redis_frame):

        img_bytes = redis_frame[1][b'image']
        img_io = io.BytesIO(img_bytes)
        img = Image.open(img_io)
        
        return img

    def _save_img(self,redis_frame,name_prefix):
        img = self._process_img(redis_frame)
        name = name_prefix + '.jpg'
        ret = img.save(name)

        self.logger.debug('img saved')
        

    def _save_gif(self,frames,name_prefix):
        gif = []
        for redis_frame in frames:
            img = self._process_img(redis_frame)
            gif.append(img)

        name = name_prefix + '.gif'
        ret = gif[0].save(name, save_all=True, optimize=False,append_images=gif[1:],loop=0)
        
        self.logger.debug('gif saved')

    def _save_video(self,redis_frames,name_prefix):

            
        name = name_prefix + '.mp4'
        #fourcc = cv2.VideoWriter_fourcc(*'avc1') 
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        #fourcc = cv2.VideoWriter_fourcc('X','2','6','5')
        fps = init['FRAME']['FPS']
        shape = (init['FRAME']['FRAME_WIDTH'], init['FRAME']['FRAME_HEIGHT'])
        
        video = cv2.VideoWriter(name,fourcc, fps, shape)
        
        np_frames = np.zeros(len(redis_frames))
        for idx, redis_frame in enumerate(redis_frames):
            frame = self._process_img(redis_frame)
            video.write(np.asarray(frame))
        
        self.logger.debug('video saved')
        


if __name__ == '__main__':
    
    host = init['DATABASE']['HOST']
    port = init['DATABASE']['PORT']
    stream_event = 'area:' + init['DATABASE']['AREA_NUMBER'] + ':events'
    dir_name = './events_area-' + init['DATABASE']['AREA_NUMBER'] + '/'
    #stream_camera = 'area:0:camera:101'
    record_duration = init['EVENTS']['RECORD_DURATION']
    e_saver = event_saver(host, port, stream_event, dir_name)
    while True:
        e_saver.xread_event(record_duration)
    
