# RedisEdge realtime video analytics web server
import argparse
import cv2
import io
import numpy as np
import redis
from urllib.parse import urlparse
from PIL import Image, ImageDraw, ImageFont
from flask import Flask, render_template, Response, send_from_directory
import funzioni
import logging
from time import time
import json
from collections import OrderedDict
from funzioni import loadConfiguration
import os


class RedisImageStream(object):
    def __init__(self, conn, args):
        self.conn = conn
        self.camera =  'area:' + initialParameters['DATABASE']['AREA_NUMBER'] + ':camera:' + initialParameters['DATABASE']['CAMERA_NUMBER']
        self.draw = self.camera + ':draw'
        self.field = args.field.encode('utf-8') 
        self.authDict =  'area:0:idVsAuth'

        self.TAG_TRANSMISSION_DELAY_MSEC = initialParameters['DRAWING_WITH_TAG']['TAG_TRANSMISSION_DELAY_MSEC'] 
        self.ARBITRARY_DELAY = initialParameters['DRAWING_WITH_TAG']['ARBITRARY_DELAY']


        fontPath = './font/FreeMonoBold.ttf'
        self.idFontSize = 10
        self.idFont = ImageFont.truetype(fontPath,self.idFontSize)

        tagFontSize = 10
        self.tagFont = ImageFont.truetype(fontPath,tagFontSize)

        self.coloreTagNonAssociato = 'white'
        self.coloreTagAssociato = 'white'

        self.counterMax = 50
        self.greenCounter = OrderedDict()
        self.redCounter = OrderedDict()

        # codici eventi
        self.greenIn = initialParameters['EVENTS']['GREEN_IN']
        self.greenOut = initialParameters['EVENTS']['GREEN_OUT']
        self.redIn = initialParameters['EVENTS']['RED_IN']
        self.redOut = initialParameters['EVENTS']['RED_OUT']

        self.pointsGreen = [(initialParameters['EVENTS']['GREEN_LINE_X1'],initialParameters['EVENTS']['GREEN_LINE_Y1']) ,(initialParameters['EVENTS']['GREEN_LINE_X2'],initialParameters['EVENTS']['GREEN_LINE_Y2'])]
        self.pointsRed = [(initialParameters['EVENTS']['RED_LINE_X1'],initialParameters['EVENTS']['RED_LINE_Y1']) ,(initialParameters['EVENTS']['RED_LINE_X2'],initialParameters['EVENTS']['RED_LINE_Y2'])]
    
        self.lines_width_counter = {}
        self.boxes_width_counter = {}
        self.bold_lines_time_delta = 1 # second

        # BADGE READERS PARAMETERS
        self.badgeReader = {}
        self.color = {}
        self.twoPointListInnerBadge = {}
        self.twoPointListOuterBadge = {}
        for reader in initialParameters['BADGE_READER'].items():
            _id = reader[1]['ID']
            self.badgeReader[_id] = (reader[1]['X']*initialParameters['FRAME']['FRAME_WIDTH'], reader[1]['Y']*initialParameters['FRAME']['FRAME_HEIGHT'])
            self.color[_id] = tuple(np.random.choice(range(256), size=3)) 

            # BADGE CIRCLE INFO
            (bX,bY) = self.badgeReader[_id]

            badge_r = 20
            leftUpPoint = (bX-badge_r, bY-badge_r)
            rightDownPoint = (bX+badge_r, bY+badge_r)
            self.twoPointListOuterBadge[_id] = [leftUpPoint, rightDownPoint]
                    
            badge_r = 2
            leftUpPoint = (bX-badge_r, bY-badge_r)
            rightDownPoint = (bX+badge_r, bY+badge_r)
            self.twoPointListInnerBadge[_id] = [leftUpPoint, rightDownPoint] 

        # GESTIONE LINEE
        for line in lines.items():
            self.lines_width_counter[line[0]] = 0.
        # GESTIONE AREE
        for box_area_id, _ in box_areas.items():
            self.boxes_width_counter[box_area_id] = 0.     


        # GRASSETTO BOX
        self.delta = {}

        self.prev_time = 0.   



    def get_last(self):
        ''' Gets latest from camera and model '''

        t = int(time()*1e3) - self.TAG_TRANSMISSION_DELAY_MSEC

        cmsg = self.conn.xrange(self.camera, min=t, max='+', count=1)

        #for id, delta in self.delta.items():
        #    if time() - delta > 1000:
        #        del self.delta[id]

        if cmsg:

            last_id = cmsg[0][0].decode('utf-8')
            label = f'{self.camera}:{last_id}'
            data = io.BytesIO(cmsg[0][1][self.field])
            img = Image.open(data)
            
            t_det = int(last_id[:-2]) + self.TAG_TRANSMISSION_DELAY_MSEC - self.ARBITRARY_DELAY
            dmsg = self.conn.xrevrange(self.draw, min=(t_det-500), count=1)

                    
            draw = ImageDraw.Draw(img, "RGBA")             

            # draw badge reader            
            for id, _ in self.badgeReader.items():   
                draw.ellipse(self.twoPointListOuterBadge[id],outline=self.color[id],width=2)          
                draw.ellipse( self.twoPointListInnerBadge[id],outline=self.color[id],width=4)   

            # DRAW LINES
            for line_id, line in lines.items():
                width = 1                          
                delta = time() - self.lines_width_counter[line_id]
                if delta < self.bold_lines_time_delta:
                    width = 4  
                    #box_width = 4            
                draw.line(line[1],fill=line[0],width=width)

            # DRAW BOXES
            for box_area_id, box_area in box_areas.items():                
                width = 1
                fill = None                
                delta = time() - self.boxes_width_counter[box_area_id]
                if delta < self.bold_lines_time_delta:
                    width = 3                                
                draw.rectangle(box_area[1], fill=fill, outline=box_area[0], width=width) # box                  
            
            # DRAW AREAS
            for area in areas.items():
                #print(line)
                coordinates = area[1][1]
                #print(coordinates)
                draw.polygon(coordinates, outline=area[1][0])
            events = OrderedDict()
            IDs = []

            if dmsg:

                #print(dmsg)
                
                detections = eval(dmsg[0][1][b'detections'].decode('utf-8'))
                tags = eval(dmsg[0][1][b'tags'].decode('utf-8'))
                pairsID = eval(dmsg[0][1][b'pairsID'].decode('utf-8'))
                PairsTag = eval(dmsg[0][1][b'pairsTag'].decode('utf-8'))
                pairsAuth = eval(dmsg[0][1][b'authCode'].decode('utf-8'))

                pairs = OrderedDict(zip(pairsID,PairsTag))
                pairsAuth = OrderedDict(zip(pairsID,pairsAuth))

                tagN = int(tags[b'tagN'])
                tagPos = tags[b'tagPos']
                tagID = tags[b'tagID']   
                tag_auth = tags[b'tagAuth']            

                if tags:
                    
                    #print(tagN)
                    for tagId in range(tagN):                             
                        tagX = tagPos[2*tagId]
                        tagY = tagPos[2*tagId+1]

                        r = 5
                        leftUpPoint = (tagX-r, tagY-r)
                        rightDownPoint = (tagX+r, tagY+r)
                        tagCoordinates = [ leftUpPoint, rightDownPoint ]
                        #print(twoPointList)
                        tag_color = initialParameters['BOX_COLORS'][str(tag_auth[tagId])]['OUTLINE']

                        draw.ellipse(tagCoordinates, outline=(self.coloreTagNonAssociato), fill=tag_color)  

                if detections:
                    
                    boxes = eval(detections[b'boxes'])
                    people = int(detections[b'people'])
                    IDs = eval(detections[b'IDs'])
                    feet = eval(detections[b'feet'])
                    events = eval(dmsg[0][1][b'events'].decode('utf-8'))

                    for box in range(people):  # Draw boxes
                        
                        # IF LINE IS CROSSED
                        box_width_lines = 1
                        for line_id, line in lines.items():
                            if line[2] in events[IDs[box]] or line[3] in events[IDs[box]]:
                                box_width_lines = 4
                                self.lines_width_counter[line_id] = time() 
                                self.delta[IDs[box]] = time()
                                box_width = 4
                                break
                            else:
                                if IDs[box] in self.delta and line_id in self.lines_width_counter:
                                    
                                    box_delta = time() -  self.delta[IDs[box]]
                                    if box_delta < self.bold_lines_time_delta:
                                        box_width_lines = 4
                                        break
                                    else:
                                        del self.delta[IDs[box]]

                        # IF BOX AREA IS CROSSED      
                        box_width_box_areas = 1                 
                        for box_area_id, box_area in box_areas.items():
                            if box_area[2] in events[IDs[box]] or box_area[3] in events[IDs[box]]:
                                self.boxes_width_counter[box_area_id] = time()  
                                box_width_box_areas = 4   
                                box_width = 4
                                self.delta[IDs[box]] = time()    
                                break      
                            else:
                                if IDs[box] in self.delta and box_area_id in self.boxes_width_counter:
                                    box_delta = time() - self.delta[IDs[box]]
                                    if box_delta < self.bold_lines_time_delta:
                                        box_width_box_areas = 4   
                                        break 
                                    else:
                                        del self.delta[IDs[box]]                       

                        box_width = max(box_width_lines,box_width_box_areas)

                        x1 = boxes[box*4]
                        y1 = boxes[box*4+1]
                        x2 = boxes[box*4+2]
                        y2 = boxes[box*4+3]

                        # Centroid
                        cX, cY = funzioni.centroid(x1,y1,x2,y2)
                        r = 3
                        leftUpPoint = (cX-r, cY-r)
                        rightDownPoint = (cX+r, cY+r)
                        twoPointListC = [leftUpPoint, rightDownPoint]

                        # Feet
                        feetCoordinatesX = float(feet[box*2])
                        feetCoordinatesY = float(feet[box*2+1])

                        # ID                        
                        r = 3
                        leftUpPoint = (feetCoordinatesX-r, feetCoordinatesY-r)
                        rightDownPoint = (feetCoordinatesX+r, feetCoordinatesY+r)
                        twoPointList = [ leftUpPoint, rightDownPoint ]
                        
                        # COLORI IN BASE ALL' AUTORIZZAZIONE
                        diz = self.conn.hget(self.authDict,IDs[box])
                        if diz:
                            auth_code = json.loads(diz)['auth']  
                            param = initialParameters['BOX_COLORS'][auth_code]   
                        else:
                            param = initialParameters['BOX_COLORS']['']   

                        outline, box_color, text_color = param['OUTLINE'], eval(param['BOX_COLOR']), eval(param['TEXT_COLOR'])               

                        # DISEGNI PER TAG E ID ASSOCIATI   
                        if IDs[box] in pairs:
                            matchingTag = pairs[IDs[box]]

                            if matchingTag in tagID:
                                tagIdx = tagID.index(matchingTag)
                                tagX = tagPos[2*tagIdx]
                                tagY = tagPos[2*tagIdx+1]

                                draw.line([(cX,cY),(tagX,tagY)],fill=self.coloreTagAssociato)

                                r = 4
                                leftUpPoint = (tagX-r, tagY-r)
                                rightDownPoint = (tagX+r, tagY+r)
                                tagCoordinates = [ leftUpPoint, rightDownPoint ]
                                draw.ellipse(tagCoordinates, fill=box_color)           

                            
                        draw.rectangle(((x1, y1), (x2, y2)), width=box_width, outline=box_color) # box
                        draw.ellipse(twoPointListC, outline='cyan', fill=box_color) # box centroid
                        draw.ellipse(twoPointList, fill=('cyan'),outline='black') # box feet

                        
                        idString = 'ID:{0:05d}'.format(IDs[box])
                        text_size = self.idFont.getsize(idString)
                        draw.rectangle(((x1+5, y1 -text_size[1]/2),(x1+5+text_size[0],y1  +text_size[1]/2)), width=box_width, outline=outline,fill=box_color) # rettangolo id
                        draw.text((x1 + 5, y1 -self.idFontSize/2), idString, font=self.idFont, fill=text_color,outline=outline) # box id

                        if IDs[box] in pairs:
                            tagString = 'T:{0}'.format(pairs[IDs[box]])
                            text_size = self.idFont.getsize(tagString)
                            draw.rectangle(((x1+5, y2 - text_size[1]/2),(x1+5+text_size[0],y2  +text_size[1]/2)), width=box_width, outline=outline,fill=box_color) # rettangolo id
                            draw.text((x1 + 5, y2 -self.idFontSize/2), tagString, font=self.tagFont,fill=text_color)

                            #print(twoPointList)
                
                else:
                    boxes = []
                    people = 0
                    IDs = []
                    feet = []
                    events = OrderedDict()

            arr = np.array(img.resize((1200,900)))
            arr = cv2.cvtColor(arr, cv2.COLOR_BGR2RGB)
            cv2.rectangle(arr, (295,900), (735,877), (0,0,0,0), thickness=-1)
            cv2.putText(arr, label, (300, 895), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,183,235), 1, cv2.LINE_AA)

            #act_time = int(time())
            #if act_time%2==0 and act_time != self.prev_time:
            #    cv2.imwrite('immagini_feet_detection/'+label+'.jpg',arr)
            #    self.prev_time = act_time

            ret, img = cv2.imencode('.jpg', arr)
            

            return img.tobytes()
        else:
            # TODO: put an 'we're experiencing technical difficlties' image
            blank_image = np.zeros((initialParameters['FRAME']['FRAME_HEIGHT'],initialParameters['FRAME']['FRAME_WIDTH'],3), np.uint8)
            return blank_image.tobytes()
        #p.xrevrange(self.camera, count=1)  # Latest frame


def gen(stream):
    while True:
        frame = stream.get_last()
        yield (b'--frame\r\n'
               b'Pragma-directive: no-cache\r\n'
               b'Cache-directive: no-cache\r\n'
               b'Cache-control: no-cache\r\n'
               b'Pragma: no-cache\r\n'
               b'Expires: 0\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

conn = None
args = None
app = Flask(__name__)

@app.route('/video')
def video_feed():
    return Response(gen(RedisImageStream(conn, args)),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    initialParameters = loadConfiguration()
    parser = argparse.ArgumentParser()
    parser.add_argument('--field', help='Image field name', type=str, default='image')
    parser.add_argument('--fmt', help='Frame storage format', type=str, default='.jpg')
    parser.add_argument('-u', '--url', help='Redis URL', type=str, default='redis://127.0.0.1:6379')
    args = parser.parse_args()

    # Load lines
    frame_width = initialParameters['FRAME']['FRAME_WIDTH']
    frame_height = initialParameters['FRAME']['FRAME_HEIGHT']
    lines = {}
    for data in initialParameters['EVENTS']['LINES'].items():
        line = data[1]
        lines[line['ID']] = (   line['COLOR'],
                                [line['X1']*frame_width,line['Y1']*frame_height, line['X2']*frame_width,line['Y2']*frame_height],
                                line['A_TO_B_CODE'],
                                line['B_TO_A_CODE']
                                )

    # Load Boxes
    box_areas = {}
    for data in initialParameters['EVENTS']['BOXES'].items():
        box = data[1]
        #print(box)
        box_areas[box['ID']] = (    box['COLOR'],
                                    [box['X1']*frame_width, box['Y1']*frame_height, box['X2']*frame_width, box['Y2']*frame_height],
                                    box['CROSS_IN_CODE'],
                                    box['CROSS_OUT_CODE'],
                                    box['AUTH_REQUESTED'],
                                    box['IS_IN_CODE'],
                            )


    areas = {}
    for data in initialParameters['EVENTS']['AREAS'].items():
        area = data[1]
        coordinates = [ (x[0] * frame_width ,x[1] * frame_height) for x in area['COORDINATES'] ]
        #print(coordinates)
        areas[line['ID']] = (  area['COLOR'],
                                coordinates,
                                area['CROSS_IN_CODE'],
                                area['CROSS_OUT_CODE'],
                                area['AUTH_REQUESTED'],
                                area['IS_IN_CODE']
                            )

    # Set up Redis connection
    if os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', False):
        url = urlparse(args.url)    
        #url = urlparse(initialParameters['DBURL'])
    else:
        url = urlparse('redis://' + initialParameters['DATABASE']['HOST'] + ':' + initialParameters['DATABASE']['PORT'])
    conn = redis.Redis(host=url.hostname, port=url.port)
    if not conn.ping():
        raise Exception('Redis unavailable')
    
    app.run(host='0.0.0.0',port=initialParameters['DRAWING_WITH_TAG']['SERVER_OUTPUT_PORT'])
    app.send_static_file('favicon.ico')
