import argparse
import time
from pathlib import Path

import cv2
#import torch
#import torch.backends.cudnn as cudnn
from numpy import random
import json

from models.experimental import attempt_load
from utils.datasets import letterbox
from utils.general import check_img_size, check_requirements, check_imshow, non_max_suppression, apply_classifier, \
    scale_coords, xyxy2xywh, strip_optimizer, set_logging, increment_path
from utils.plots import plot_one_box
from utils.torch_utils import select_device, load_classifier, time_synchronized

import utils.camera_parameters_d as C_d
#from tensorflow import keras
import redis
import json
#from walrus import Database
import redis
import time as t
import numpy as np
import re
import struct

# A Redis gear for orchestrating realtime video analytics
import io
import cv2
#import redisAI
import numpy as np
from time import time
from PIL import Image

class redisDB:
    def __init__(self,host,port, inputStream, outputStream, outputFeet, streamBadge,authDict):
        #self.r_data = Database(host=host, port=port, db=0)
        self.r_data = redis.Redis(host=host, port=port)
        if not self.r_data.ping():
            raise Exception('Redis unavailable')

        # stream in lettura dei dati salvati da rtsp2redis.py
        self.streamName = inputStream # data
        
        self.detectionsStreamName = outputStream
        self.feetStreamName = outputFeet

        #badge 
        self.streamBadge = streamBadge
        self.badgeStreamID = int(time()*1e3)

        # Authorization
        self.authDict = authDict
    
    def detection2redisDB(self, detections, feet):
        p = self.r_data.pipeline()
        p.xadd(self.detectionsStreamName,detections,maxlen=10000)
        #p.xadd(self.feetStreamName,feet,maxlen=1000)
        p.publish(self.feetStreamName,feet)
        p.execute()
        return

    def detections2RedisNew(self, detections, feet):
        p = self.r_data.pipeline()
        p.xadd(self.detectionsStreamName,detections,maxlen=10000)
        #p.xadd(self.feetStreamName,feet,maxlemen=1000)
        p.publish(self.feetStreamName,json.dumps(feet))
        p.execute()
        return

    def getFrameFromRedis(self):  

        try:
            
            p = self.r_data.pipeline()
            p.xrevrange(self.streamName,count=1)
            stream_value = p.execute()

            streamID = stream_value[0][0][0]
            frame = stream_value[0][0][1]
        except Exception as e:
            print('getFrameFromRedis exception: {}'.format(e))
            return False,False ,False #, False
        else:
            return True, streamID, frame #, beacons

    def getBadgeEvent(self):

        #print('getBadgeEvent')

        #self.badgeStreamID = badge_id = reader_id = False

        #badgeMessage = self.r_data.xread({self.streamBadge:self.badgeStreamID},count=1)
        badgeMessage = self.r_data.xread({'e:areas:10:badges:events':self.badgeStreamID},count=1)
        #print('getBadgeEvent:\t{}'.format(badgeMessage))
        
        if badgeMessage:
            print('badgiata ricevuta: {}'.format(badgeMessage))
            badge = badgeMessage[0][1][0]
            self.badgeStreamID = badge[0]
            badge_id = eval(badge[1][b'data'])['badge_id']
            reader_id = eval(badge[1][b'data'])['reader_id']

            return self.badgeStreamID,badge_id,reader_id
        else:
            return False, '', ''

    def updateAuthDict(self, id, auth, isBadge):
        '''
        Update the dictionary id -> (authorization Code, isBadge)

        Arguments:
            -   id: id of the badging person from the detection
            -   auth: authorization code
            -   isBadge: bool to check if the authorization come from badge
        '''
        data = {
            "auth":auth,
            "isBadge":isBadge
        }

        dictName = self.authDict 
        ttl = 1800 # seconds
        self.r_data.hset(name=dictName,key=id,value=json.dumps(data))
        self.r_data.expire(name=dictName, time=ttl)

    def getAuthDictID(self, id):
        '''
        Get valuesof corresponding id from authorization dictionary

        Arguments:
            - id: id whose value is required

        Returns:
            - auth: authorization code

        '''
        auth = self.r_data.hget(self.authDict, id)

        return auth
        
    def getAuthDict(self):
        '''
        Get all values from authorization dictionary
        '''
        return self.r_data.hgetall(self.authDict)

    def getAuthDictID(self,id):
        '''
        Get id value from authorization dictionary
        '''
        auth = self.r_data.hget(self.authDict, str(id))
        
        if auth is None:
            return False
        else:
            auth = json.loads(auth)
            return auth
    
    def get_last_id(self):
        last_message = self.r_data.hgetall(self.authDict)
        #print('last_message: {}'.format(last_message))
        if last_message:            
            last_id = max(last_message, key=int)
            return last_id
        else:
            return -1 

class LoadImages777:  # for inference
    def __init__(self, im0s, img_size=640, stride=32):
        ni, nv = 1, 0

        self.img_size = img_size
        self.stride = stride
        #self.files = images + videos
        self.immagine = [im0s]
        self.nf = ni + nv  # number of files
        self.video_flag = [False] * ni + [True] * nv
        self.mode = 'image'
        #if any(videos):
        #    self.new_video(videos[0])  # new video
        #else:
        self.cap = None
        #assert self.nf > 0, f'No images or videos found in {p}. ' \
        #                    f'Supported formats are:\nimages: {img_formats}\nvideos: {vid_formats}'

    def __iter__(self):
        self.count = 0
        return self

    def __next__(self):
        if self.count == self.nf:
            raise StopIteration
        immagine = self.immagine[self.count]

        if self.video_flag[self.count]:
            # Read video
            self.mode = 'video'
            ret_val, img0 = self.cap.read()
            if not ret_val:
                self.count += 1
                self.cap.release()
                if self.count == self.nf:  # last video
                    raise StopIteration
                else:
                    path = self.files[self.count]
                    self.new_video(path)
                    ret_val, img0 = self.cap.read()

            self.frame += 1
            print(f'video {self.count + 1}/{self.nf} ({self.frame}/{self.nframes}) {path}: ', end='')

        else:
            # Read image
            self.count += 1
            img0 = immagine  # BGR
            assert img0 is not None, 'Image Not Found '
            #print(f'image {self.count}/{self.nf} {path}: ', end='')

        # Padded resize
        img = img0 #letterbox(img0, self.img_size, stride=self.stride)[0]

        # Convert
        img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        img = np.ascontiguousarray(img)

        return "path", img, img0, self.cap

    def new_video(self, path):
        self.frame = 0
        self.cap = cv2.VideoCapture(path)
        self.nframes = int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT))

    def __len__(self):
        return self.nf  # number of files

if __name__ == '__main__':

    host = '127.0.0.1'
    port = '6379'
    inputStream = ''
    outputStream = ''
    outputFeet = ''
    streamBadge = 'e:areas:10:badges:events'
    db = redisDB(host,port, inputStream, outputStream, outputFeet, streamBadge)
    t0 = time()
    badgeStreamID, badge_id,badge_reader_id =  db.getBadgeEvent()
    t1 = time()
    print(t1-t0)

    #streamID, buf = frame[b'image']
    #nparr = np.frombuffer(buf, np.uint8)
    #img = cv2.imdecode(nparr,cv2.IMREAD_UNCHANGED)
    #print(frame[b'streamId'])
    #print(x,"\t",y)
    #cv2.imshow('frm',img)
    #cv2.waitKey(0)
    #print(frame)
    #downsampleStream(x)
    #process_image(img, height)