import yaml 

def loadConfiguration(file_path = "./initialization.yaml"):
    '''
    Load configuration parameters from yaml file.

    Arguments:
        - file_path: path to the YAML file.
    
    Returns:
        - None

    Note:
        - exit if loading fails
    '''

    with open(file_path, 'r') as config:
            try:
                data = yaml.safe_load(config)
                return data

            except yaml.YAMLError as exc:
                print(exc)
                exit

  