# RedisEdge realtime video analytics video capture script
import argparse
import cv2
import redis
import time
from urllib.parse import urlparse
from funzioni import loadConfiguration, showExceptionInfo
import os
import logging
import logging.config
import yaml
import sys

initialParameters = loadConfiguration()
#cv2.CAP_PROP_XI_TIMEOUT = 1000

class SimpleMovingAverage(object):
    ''' Simple moving average '''
    def __init__(self, value=0.0, count=7):
        self.count = int(count)
        self.current = float(value)
        self.samples = [self.current] * self.count

    def __str__(self):
        return str(round(self.current, 3))

    def add(self, value):
        v = float(value)
        self.samples.insert(0, v)
        o = self.samples.pop()
        self.current = self.current + (v-o)/self.count

class Video:
    def __init__(self, infile=0, fps=30.0):
        while True:
            try:
                self.isFile = not str(infile).isdecimal()
                #print('isFile: {}'.format(self.isFile))
                self.ts = time.time()
                self.infile = infile
                self.cam = cv2.VideoCapture(self.infile)
                
                if self.cam is None or not self.cam.isOpened():
                    raise Exception('Can\'t find camera! Check connections.')
                if not self.isFile:
                    self.cam.set(cv2.CAP_PROP_FPS, fps)
                    self.fps = fps
                    # TODO: some cameras don't respect the fps directive
                    self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, initialParameters['FRAME']['FRAME_WIDTH'])
                    self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, initialParameters['FRAME']['FRAME_HEIGHT'])
                else:
                    self.fps = self.cam.get(cv2.CAP_PROP_FPS)
                    self.sma = SimpleMovingAverage(value=0.1, count=19)
                break
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                debug_logger.warning('{}\t|\t{}\t|\t{}\t|\t{}'.format(exc_type,fname, exc_tb.tb_lineno,e))
                print('can\'t find camera... Retry every 30 seconds.')
                time.sleep(30)
 
    def __iter__(self):
        self.count = -1
        return self

    def __next__(self):
        
        self.count += 1
        
        if self.isFile:
            delta = time.time() - self.ts
            self.sma.add(delta)
            #print('self.sma.current: {}\tself.fps: {}\tsleep: {}'.format(self.sma.current,self.fps,max(0,(1.0 - self.sma.current*self.fps)/self.fps)))
            time.sleep(max(0,(1.0 - self.sma.current*self.fps)/self.fps))
            self.ts = time.time()
            
        ret_val, img0 = self.cam.read()
        #time2 = time.time()
        if not ret_val and self.isFile:
            self.cam.set(cv2.CAP_PROP_POS_FRAMES, 0)
            ret_val, img0 = self.cam.read()
            if not ret_val:
                self.cam.release()
                raise Exception('Camera connection lost. Reinitialize...')
                
        img = img0
        if not self.isFile:
            img = cv2.flip(img, 1)

        return ret_val,self.count, img

    def __len__(self):
        return 0

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', help='Input file (leave empty to use webcam)', nargs='?', type=str, default=initialParameters['FRAME']['INPUT_STREAM'])
    parser.add_argument('-a', '--area', help='Area identifying number', type=str, default=initialParameters['DATABASE']['AREA_NUMBER'])
    parser.add_argument('-c', '--camera', help='Camera identifying number', type=str, default=initialParameters['DATABASE']['CAMERA_NUMBER'])
    parser.add_argument('-u', '--url', help='Redis URL', type=str, default='redis://127.0.0.1:'+initialParameters['DATABASE']['PORT'])
    parser.add_argument('-w', '--webcam', help='Webcam device number', type=int, default=0)
    parser.add_argument('-v', '--verbose', help='Verbose output',action='store_true', default=False)
    parser.add_argument('--count', help='Count of frames to capture', type=int, default=None)
    parser.add_argument('--fmt', help='Frame storage format', type=str, default='.jpg')
    parser.add_argument('--fps', help='Frames per second (webcam)', type=float, default=initialParameters['FRAME']['FPS'])
    parser.add_argument('--maxlen', help='Maximum length of output stream', type=int, default=10000)
    args = parser.parse_args()

    outputFrame = 'area:' + args.area + ':camera:' + args.camera
    outputFrameInfo = outputFrame + ':info'
    
    # Environment settings
    if os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', False):
        url = urlparse(args.url)    
    else:
        url = urlparse('redis://' + initialParameters['DATABASE']['HOST'] + ':' + initialParameters['DATABASE']['PORT'])
            
        print('\nConfiguration parameters:')
        for name, value in initialParameters.items():
            print('\t{0:.<20}{1}'.format(name,value))
        print()

    # Logging
    with open('./capture_log_config.yaml', 'r') as f:
        try:
            config = yaml.safe_load(f.read())
            logging.config.dictConfig(config)

            debug_logger = logging.getLogger('capture_logger_debug')
            debug_logger.setLevel('DEBUG')
            debug_logger.debug('New Execution')

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            debug_logger.warning('{}\t|\t{}\t|\t{}\t|\t{}'.format(exc_type,fname, exc_tb.tb_lineno,'Can\'t find logginf configuration file. Exit.'))
            exit()

    # Redis
    try:
        conn = redis.Redis(host=url.hostname, port=url.port)
        if not conn.ping():
            raise Exception('Redis unavailable')
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        debug_logger.warning('{}\t|\t{}\t|\t{}\t|\t{}'.format(exc_type,fname, exc_tb.tb_lineno,'Redis unavailable'))

    # Load video      
    loader = Video(infile=args.infile, fps=args.fps)

    fps = 0
    prevID = 0
    prevCount = 0
    my_loader = iter(loader)

    # Start frames processing
    while True:

        try:

            (ret_val, count, img) = next(my_loader)

            _, data = cv2.imencode(args.fmt, img)
            msg = {
                'count': count,
                'image': data.tobytes()
            }
            _id = conn.xadd(outputFrame, msg, maxlen=args.maxlen)

            # TODO: migliorare lo stream di info facendo uso dei profiler. Capire se conviene farlo qui o altrove ( ci sta monitorare il frame rate per fare downsampling come viene fatto in gear.py)
            if count%50:
                id = int(_id.decode("utf-8").split('-')[0]) / 1e3
                fps = (count - prevCount) / (id - prevID)

                prevID = id
                prevCount = count

            msgInfo = {
                'id': _id,
                'frameShape': str(img.shape),
                'fps': fps
            }
            conn.xadd(outputFrameInfo,msgInfo,maxlen=args.maxlen)

            if args.verbose:
                print('frame: {} id: {}'.format(count, _id))
            if args.count is not None and count+1 == args.count:
                print('Stopping after {} frames.'.format(count))
                break

        except Exception as e:

            #self.logger.error('Exception in Pairing', exc_info=True)
            showExceptionInfo(e,'Capture')

            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            debug_logger.warning('{}\t|\t{}\t|\t{}\t|\t{}'.format(exc_type,fname, exc_tb.tb_lineno,e))

            del loader
            del my_loader
            loader = Video(infile=args.infile, fps=args.fps)
            my_loader = iter(loader)
