import redis
from time import time
import time as t
from funzioni import camera
import sys
import os
import argparse
from urllib.parse import urlparse
from funzioni import loadConfiguration
import redisManager_new as RM
import logging
import logging.config
import yaml

class msg:
    """Redis message reader"""

    def __init__(self, url):

        self.db = RM.redis_manager(url.hostname, url.port)

        prefix = 'area:' + initialParameters['DATABASE']['AREA_NUMBER'] + ':camera:' + initialParameters['DATABASE']['CAMERA_NUMBER']
        self.streamDetections = prefix + ':yolo'
        self.streamBeacons = initialParameters['DATABASE']['STREAM_BEACON']
        self.streamTagDetections = prefix + ':tagDet'

        self.reception_ts = '$'
        
        self.TAG_TRANSMISSION_DELAY_MSEC = initialParameters['TIME_SYNC']['TAG_TRANSMISSION_DELAY_MSEC'] # Delta temporale con cui vado indietro a cercare nelle detections
        self.RITARDO_AGGIUNTIVO = initialParameters['TIME_SYNC']['RITARDO_AGGIUNTIVO'] # ms
        self.MAX_TIME_DISTANCE = initialParameters['TIME_SYNC']['MAX_TIME_DISTANCE'] # ms

        self.f = open(initialParameters['TIME_SYNC']['DELAYS_FILE'], 'a')

        self.frame_ts = 0
        self.tags = []

        self.camera = camera(initialParameters)

        with open('./pair_log_config.yaml', 'r') as f:
            config = yaml.safe_load(f.read())
            logging.config.dictConfig(config)
        self.logger = logging.getLogger('coupling_logger_debug')
        self.logger.setLevel('DEBUG')
        self.log_accepted_rateo = logging.getLogger('accepted_rejected')
        self.log_accepted_rateo.setLevel('DEBUG')

        self.dets_received_ts = 0
        self.tags_received_ts = 0


        
    def __iter__(self):
        return self

    def __next__(self):

        t_det = int(time()*1e3) - self.TAG_TRANSMISSION_DELAY_MSEC
        
        detections_from_stream, tags_from_stream = self.db.xread_det_tag(self.streamDetections, t_det, self.streamBeacons)

        old_dets_ts = self.dets_received_ts
        if detections_from_stream:
            
            self.dets_received_ts = detections_from_stream[0][1][0][0]
            detections_to_output = detections_from_stream[0][1][0][1]
            
        else:
            detections_to_output = {}


        tagID = []
        tagPos = []
        tagAuth = []
        old_tags_ts = self.tags_received_ts
        if tags_from_stream:
            #print(tags_from_stream)
            self.tags_received_ts = tags_from_stream[0][0]
            tags = eval(tags_from_stream[0][1][b'data'])
            #print(tags_received_ts)
            
            accepted = 0
            rejected = 0
            for tag in tags:

                tag_id = tag['tag_id']
                #tag_name = tag['tag_name']
                position_ts = tag['position_ts']
                response_ts = tag['response_ts']
                pos = self.camera.computeWorldToPixel(tag['pos_x'],tag['pos_y'])
                auth_code = tag['auth_code']

                t_tag = int(self.tags_received_ts[:-2]) - (response_ts - position_ts) - self.RITARDO_AGGIUNTIVO

                #info_ritardo = '{}\t{}\t{}'.format(self.TAG_TRANSMISSION_DELAY_MSEC,len(tags),t_det-t_tag)
                #self.logger.debug(info_ritardo)

                if abs(t_tag - t_det) < self.MAX_TIME_DISTANCE:
                    #if abs(t_tag - t_det) < self.MAX_TIME_DISTANCE*3:
                    #    self.f.write('{}\n'.format(t_tag - t_det))
                    #    self.f.flush()
                    accepted += 1
                    
                    tagID += [tag_id]
                    tagPos += [pos[0], pos[1]]
                    tagAuth += [auth_code]
                else:
                    rejected += 1

        if (old_dets_ts != self.dets_received_ts) or (old_tags_ts != self.tags_received_ts):
            tags_to_output = {

                #b'streamTagID': streamTagId,
                b'tagN' : len(tagID),
                b'tagID' : tagID,
                b'tagPos' : tagPos,
                b'tagAuth' : tagAuth

            }

            tag_det = {
                b'detections' : str(detections_to_output),
                b'tags' : str(tags_to_output)
            }

            self.db.xadd_detection_tag(self.streamTagDetections,tag_det)

if __name__ == '__main__':
    initialParameters = loadConfiguration()
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--url', help='Redis URL', type=str, default='redis://127.0.0.1:6379')
    args = parser.parse_args()

    if os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', False):
        url = urlparse(args.url)    
        #url = urlparse(initialParameters['DBURL'])
    else:
        url = urlparse('redis://' + initialParameters['DATABASE']['HOST'] + ':' + initialParameters['DATABASE']['PORT'])

    print('\nConfiguration parameters:')
    for name, value in initialParameters.items():
        print('\t{0:.<40}{1}'.format(name,value))
    print()  

    message = msg(url)

    #iterations = 0
    #time_tot = 0
    #time0 = time()
    while True:

        #iterations += 1
        #time_tot += (time() - time0)
        #if iterations%15==0:
        #    print(iterations/time_tot)
        #time0 = time()
        message.__next__()