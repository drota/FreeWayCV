# RedisEdge realtime video analytics web server
import argparse
import cv2
import io
import numpy as np
import redis
from urllib.parse import urlparse
from PIL import Image, ImageDraw, ImageFont
from flask import Flask, render_template, Response
import funzioni
from funzioni import loadConfiguration
import logging
import json

initialParameters = loadConfiguration()

class RedisImageStream(object):
    def __init__(self, conn, args):
        self.conn = conn
        self.camera = 'area:' + initialParameters['DATABASE']['AREA_NUMBER'] + ':camera:' + initialParameters['DATABASE']['CAMERA_NUMBER']
        self.boxes = self.camera + ':yolo'
        self.field = args.field.encode('utf-8') 
        #self.lastDetID = self.conn.xrevrange(self.feet, count=1)[0][0]#'$'
        self.oldBmsg = -1
        self.oldCmsg = -1

    def get_last(self):
        ''' Gets latest from camera and model '''
        #p = self.conn.pipeline()
        #p.xrevrange(self.camera, count=1)  # Latest frame
        #p.xrevrange(self.boxes, count=1)   # Latest boxes
        #p.xrevrange(self.feet, count=1)   # Latest feet
        #cmsg, bmsg, fmsg = p.execute()
        cmsg = self.conn.xrevrange(self.camera, count=1)

        if cmsg:
            self.oldCmsg = cmsg
            last_id = cmsg[0][0].decode('utf-8')
            label = f'{self.camera}:{last_id}'
            data = io.BytesIO(cmsg[0][1][self.field])
            img = Image.open(data)
            draw = ImageDraw.Draw(img)
            #self.lastDetID = cmsg[0][0]
            bmsg = self.conn.xread({self.boxes:last_id}, count=1,block=15)


            for line in lines.items():
                #print(line)
                draw.line(line[1][1],fill=line[1][0],width=1)

            for area in areas.items():
                #print(line)
                coordinates = area[1][1]
                #print(coordinates)
                draw.polygon(coordinates, outline=area[1][0])

            if bmsg:
                #print(bmsg[0][1][0][1])
                self.oldBmsg = bmsg
                boxes = np.fromstring(bmsg[0][1][0][1]['boxes'.encode('utf-8')][1:-1], sep=',')
                IDs = np.fromstring(bmsg[0][1][0][1]['IDs'.encode('utf-8')][1:-1], sep=',', dtype=int)
                label += ' people: {}'.format(bmsg[0][1][0][1]['people'.encode('utf-8')].decode('utf-8'))
                for box in range(int(bmsg[0][1][0][1]['people'.encode('utf-8')])):  # Draw boxes
                    x1 = boxes[box*4]
                    y1 = boxes[box*4+1]
                    x2 = boxes[box*4+2]
                    y2 = boxes[box*4+3]

                    draw.rectangle(((x1, y1), (x2, y2)), width=1, outline='red')

                    # Centroid
                    cX, cY = funzioni.centroid(x1,y1,x2,y2)
                    r = 3
                    leftUpPoint = (cX-r, cY-r)
                    rightDownPoint = (cX+r, cY+r)
                    twoPointList = [leftUpPoint, rightDownPoint]
                    draw.ellipse(twoPointList, fill=(255,0,0,255))
                    
                    # feet  
                    #if fmsg:
                    feet = np.fromstring(bmsg[0][1][0][1]['feet'.encode('utf-8')][1:-1], sep=',')
                    feetCoordinatesX = float(feet[box*2])
                    feetCoordinatesY = float(feet[box*2+1])
                    
                    r = 3
                    leftUpPoint = (feetCoordinatesX-r, feetCoordinatesY-r)
                    rightDownPoint = (feetCoordinatesX+r, feetCoordinatesY+r)
                    twoPointList = [ leftUpPoint, rightDownPoint ]
                    #print(twoPointList)
                    draw.ellipse(twoPointList, fill=('cyan'))

                    # ID
                    font = './font/yudit.ttf'
                    fontSize = 20
                    draw.text((x1 + 5, y1 + 5), str(IDs[box]), font=ImageFont.truetype(font,fontSize), fill=(255,0,0,255))

            arr = np.array(img)
            arr = cv2.cvtColor(arr, cv2.COLOR_BGR2RGB)
            cv2.putText(arr, label, (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,255), 1, cv2.LINE_AA)
            ret, img = cv2.imencode('.jpg', arr)
            return img.tobytes()
        else:
            # TODO: put an 'we're experiencing technical difficlties' image
            pass

def gen(stream):
    while True:
        frame = stream.get_last()
        yield (b'--frame\r\n'
               b'Pragma-directive: no-cache\r\n'
               b'Cache-directive: no-cache\r\n'
               b'Cache-control: no-cache\r\n'
               b'Pragma: no-cache\r\n'
               b'Expires: 0\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

conn = None
args = None
app = Flask(__name__)

@app.route('/video')
def video_feed():
    return Response(gen(RedisImageStream(conn, args)),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('camera', help='Input camera stream key', nargs='?', type=str, default='camera:0')
    parser.add_argument('boxes', help='Input model stream key', nargs='?', type=str, default='camera:0:yolo')
    parser.add_argument('--field', help='Image field name', type=str, default='image')
    parser.add_argument('--fmt', help='Frame storage format', type=str, default='.jpg')
    parser.add_argument('-u', '--url', help='Redis URL', type=str, default='redis://127.0.0.1:6379')
    args = parser.parse_args()

    # Load lines
    frame_width = initialParameters['FRAME']['FRAME_WIDTH']
    frame_height = initialParameters['FRAME']['FRAME_HEIGHT']
    lines = {}
    for data in initialParameters['EVENTS']['LINES'].items():
        line = data[1]
        lines[line['ID']] = (  line['COLOR'],
                                    [line['X1']*frame_width,line['Y1']*frame_height, line['X2']*frame_width,line['Y2']*frame_height],
                                    line['A_TO_B_CODE'],
                                    line['B_TO_A_CODE']
                                )

    areas = {}
    for data in initialParameters['EVENTS']['AREAS'].items():
        area = data[1]
        coordinates = [ (x[0] * frame_width ,x[1] * frame_height) for x in area['COORDINATES'] ]
        #print(coordinates)
        areas[line['ID']] = (  area['COLOR'],
                                coordinates,
                                area['CROSS_IN_CODE'],
                                area['CROSS_OUT_CODE'],
                                area['AUTH_REQUESTED'],
                                area['IS_IN_CODE']
                            )

    # Set up Redis connection
    url = urlparse(args.url)
    conn = redis.Redis(host=url.hostname, port=url.port)
    if not conn.ping():
        raise Exception('Redis unavailable')
    
    app.run(host='0.0.0.0',port=5001)
