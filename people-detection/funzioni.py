import yaml 
from scipy.interpolate import Rbf, interpn, NearestNDInterpolator, CloughTocher2DInterpolator, LinearNDInterpolator
import numpy as np
import time
import cv2

def loadConfiguration():
    with open("../initialization.yaml", 'r') as config:
            try:
                data = yaml.safe_load(config)
                return data

            except yaml.YAMLError as exc:
                print(exc)
                exit

class feet:
    def __init__(self):
        self.RbfX, self.RbfY = self.computeRbf()

    def computeRbf(self):
        #with open("./feetPosition/101_640x480.yaml", 'r') as stream:
        with open("./feetPosition/101_1200x900.yaml", 'r') as stream:
                try:
                    data = yaml.safe_load(stream)
                    self.centroidCoordinates = np.array(data['feet_calib'], dtype='f')[:,0,:]
                    self.feetCoordinates = np.array(data['feet_calib'], dtype='f')[:,1,:]

                    self.input_width = np.array(data['feet_calib_image_width'])
                    self.input_height = np.array(data['feet_calib_image_height'])

                    self.image_width = 640
                    self.image_height = 480

                    # Normalizing coordinates
                    for v in [self.centroidCoordinates,self.feetCoordinates]:
                        v[:,0] /= self.input_width
                        v[:,1] /= self.input_height
                    

                except yaml.YAMLError as exc:
                    print(exc)

        func = 'linear'
        smooth = 0
        RbfX = Rbf(self.centroidCoordinates[:,0],self.centroidCoordinates[:,1], self.feetCoordinates[:,0], function=func, smooth=smooth) # rbf instance
        RbfY = Rbf(self.centroidCoordinates[:,0],self.centroidCoordinates[:,1], self.feetCoordinates[:,1], function=func, smooth=smooth) # rbf instance

        return RbfX, RbfY

    def position(self, x1,y1,x2,y2):

        centroidX = (x1 + x2) / 2. / self.image_width
        centroidY = (y1 + y2) / 2. / self.image_height 

        feetX_norm = self.RbfX(centroidX , centroidY ) 
        feetY_norm = self.RbfY(centroidX , centroidY ) 

        feetX = feetX_norm * self.image_width
        feetY = feetY_norm * self.image_height 

        return [int(feetX), int(feetY)], (float(feetX_norm), float(feetY_norm))

    def position_new(self, x1,y1,x2,y2):

        centroidX = (x1 + x2) / 2. / self.image_width
        centroidY = (y1 + y2) / 2. / self.image_height 
        #m = cv2.estimateAffine2D(self.centroidCoordinates, self.feetCoordinates)
        #Z = cv2.getAffineTransform()

        #Z = cv2.perspective
        #print(self.centroidCoordinates.shape)
        #print(self.feetCoordinates.shape)
        #exit()
        #feet_pos = interpn(self.centroidCoordinates, self.feetCoordinates, [centroidX, centroidY])

        interp = LinearNDInterpolator(list(self.centroidCoordinates), self.feetCoordinates)

        Z = interp(centroidX, centroidY)
        return [int(Z[0]*self.image_width), int(Z[1]*self.image_height)], (float(Z[0]), float(Z[1]))

def badgeEvent(readerPosition, feetPositionList, peopleID):

    '''
    Given the reader position and the list of the feet positions,
    pairs the badge event to the right detection ID

    Arguments:
        -   readerPosition: position in pixels (rx,ry) 
            of the badge where the event has been registered
        -   feetPosition: list of the position of the feet 
            (in pixel) detected in the frame
        -   peopleID: list of the id detected in the current frame

    Returns:
        - people ID paired with badge event (if association process is ok, otherwise False)
    '''
    
    a = np.array(readerPosition) 
    b = np.zeros((len(peopleID),2))
    
    for idx in range(len(peopleID)):
        b[idx] = (feetPositionList[2*idx],feetPositionList[2*idx+1])
    if len(peopleID) > 0 and len(a)>0:
        dist = [ np.linalg.norm(a-x) for x in b]
        idx = dist.index(np.min(dist))

        if dist[idx] < 20:
            # TODO: impostare da cfg la distanza entro la quale convalidare la distanza    
            return str(peopleID[idx])
        
    return False

def badgeEvent_new(readerPosition, targets,frame_shape):

    '''
    Given the reader position and the list of the feet positions,
    pairs the badge event to the right detection ID

    Arguments:
        -   readerPosition: position in pixels (rx,ry) 
            of the badge where the event has been registered
        -   targets: list of the targets detected in the current frame

    Returns:
        - people ID paired with badge event (if association process is ok, otherwise False)
    '''
    
    reader_position = np.array(readerPosition) 
    target_feet = np.zeros((len(targets),2))

    ids = []
    
    for idx, target in enumerate(targets):
        target_feet[idx] = (target.screen_feet[0] * frame_shape[0], target.screen_feet[1] * frame_shape[1])
        ids.append(target.target_id)
        #print(target_feet[idx])
        
    if len(targets) > 0 and len(target_feet)>0:
        dist = [ np.linalg.norm(reader_position-x) for x in target_feet]
        idx = dist.index(np.min(dist))

        if dist[idx] < 20:
            # TODO: impostare da cfg la distanza entro la quale convalidare la distanza    
            return str(ids[idx]), idx
        
    return False, False




if __name__ == '__main__':

    #from numpy.linalg import inv

    #cam = camera()


    #readerPosition = (10,10)
    #peopleID = [0,1,235,3]
    #feetPositionList = [0,0,20,20,10,10,30,30]
    #t0 = time.time()
    #b = badgeEvent(readerPosition, feetPositionList, peopleID)
    #print(b)
    pass