import redis
from time import time
import numpy as np
from scipy.spatial import distance as dist
from scipy.optimize import linear_sum_assignment
from collections import OrderedDict
from funzioni import showExceptionInfo, loadConfiguration, evaluateLineParameters,isUnder, clearOrderedDict, Box
import argparse
from urllib.parse import urlparse
import os
import sys
import logging
import logging.config
import yaml
import io
from PIL import Image
from redisManager import redis_manager
from eventHandler import eventHandler
import json
import target_management_rt as tm

class pairing:
    """object that pairs people detection and beacon tags"""

    def __init__(self, url):

        with open('./pair_log_config.yaml', 'r') as f:
            config = yaml.safe_load(f.read())
            logging.config.dictConfig(config)

        self.logger = logging.getLogger('pair_logger_debug')
        self.logger.debug('New Execution')

        self.connection = redis_manager()
        self.db = self.connection.connect(url.hostname, url.port)

        self.streamFrame = 'area:' + initialParameters['DATABASE']['AREA_NUMBER'] + ':camera:' + initialParameters['DATABASE']['CAMERA_NUMBER']
        self.streamTagDetections = self.streamFrame + ':tagDet'
        streamEvents = 'area:' + initialParameters['DATABASE']['AREA_NUMBER'] + ':events'
        self.lastID = '$'
        self.backupFeetPos = OrderedDict()

        self.eventHandler = eventHandler(streamEvents,url.hostname,url.port, initialParameters['DATABASE']['CAMERA_NUMBER'])

        # equazioni delle linee per la registrazione degli eventi
        frame_width = initialParameters['FRAME']['FRAME_WIDTH']
        frame_height = initialParameters['FRAME']['FRAME_HEIGHT']
        self.img_shape = np.array((frame_width, frame_height))
        pointsGreen = [(initialParameters['EVENTS']['GREEN_LINE_X1'],initialParameters['EVENTS']['GREEN_LINE_Y1']) ,(initialParameters['EVENTS']['GREEN_LINE_X2'],initialParameters['EVENTS']['GREEN_LINE_Y2'])]
        pointsRed = [(initialParameters['EVENTS']['RED_LINE_X1'],initialParameters['EVENTS']['RED_LINE_Y1']) ,(initialParameters['EVENTS']['RED_LINE_X2'],initialParameters['EVENTS']['RED_LINE_Y2'])]
        self.greenParam = evaluateLineParameters(pointsGreen)
        self.redParam = evaluateLineParameters(pointsRed)
        
        # current frame
        self.upperBound = np.sqrt(pow(frame_width,2) + pow(frame_height,2)) + 1 # lim sup per la distanza
        self.dMax = self.upperBound * initialParameters['PAIRING_DETECTION_TAG']['MAX_PERCENTAGE_DIST'] # distanza massima oltre la quale non considero il tag associato a nessuna detection

        # dictionary
        self.people = OrderedDict() # dizionario che associa le persone all' id del tag
        self.disappeared = OrderedDict()
        self.authCode = OrderedDict()
        self.eventsHistory = OrderedDict() # registro degli eventi per un dato id
        self.maxDisappeared = initialParameters['PAIRING_DETECTION_TAG']['MAX_DISAPPEARED']

        # codici eventi
        self.greenIn = initialParameters['EVENTS']['GREEN_IN']
        self.greenOut = initialParameters['EVENTS']['GREEN_OUT']
        self.redIn = initialParameters['EVENTS']['RED_IN']
        self.redOut = initialParameters['EVENTS']['RED_OUT']

        boxCoordinates = [400,230,500,330]
        self.box = Box(boxCoordinates,initialParameters['DATABASE']['AREA_NUMBER'],initialParameters['DATABASE']['CAMERA_NUMBER'],0)
        #passare (anche da file di configurazione) gli spigoli del box che va a definire la mia area

        self.streamDraw = self.streamFrame + ':draw'

    def __iter__(self):
        return self

    def __next__(self):
        
        try:
            combinedDetection = self.db.xread({self.streamTagDetections:self.lastID},count=1,block=0)

            if combinedDetection:
                
                newPairs,newPairsAuth = self.pairInFrame(combinedDetection)
                self.updateDict(newPairs,newPairsAuth)

                pairsID = []
                pairsTag = []
                authCode = []

                for key,value in self.people.items():
                    pairsID += [key]
                    pairsTag += [value]
                    authCode += [self.authCode[key]]                

                    # update dict for feet in rt
                    self.connection.updateAuthDict(key,self.authCode[key], False)

                #print(pairsTag)
                
                # gestione degli eventi
                detections = eval(combinedDetection[0][1][0][1][b'detections'])
                frameTS = detections[b'frame_id']
                targets = json.loads(detections[b'targets'])
                tags = combinedDetection[0][1][0][1][b'tags']
                feetPos = OrderedDict()

                output_targets = []
                for idx,target in enumerate(targets):
                    
                    target_id = target['target_id']
                    feetPos[target_id] = np.array(target['screen_feet']) * self.img_shape
                    
                    events = []
                    # check if an event occurs
                    if target_id in self.backupFeetPos:
                        if target_id not in self.eventsHistory:
                            self.eventsHistory[target_id] = []

                        # TODO: qua dovrei mettere un ciclo su tutte le linee che ho nel file di configurazione, sia per le linee che per le aree
                        # TODO: ovviamente quando modifico qua dovrei riguardare anche la parte di server per il disegno e la gestione delle aree

                        # EVENTI LINEE
                        if isUnder(feetPos[target_id], self.greenParam) and not isUnder(self.backupFeetPos[target_id], self.greenParam):
                            events += [self.greenIn]# ingresso verde
                            self.eventsHistory[target_id].append(self.greenIn)

                            args = [self.greenIn,frameTS]
                            if target_id in self.people:
                                args += [self.people[target_id], self.authCode[target_id]]                              
                            self.eventHandler.registerEvent(*args)
                            #self.recordEventGIF()

                        elif not isUnder(feetPos[target_id], self.greenParam) and isUnder(self.backupFeetPos[target_id], self.greenParam):
                            events += [self.greenOut] # uscita verde
                            self.eventsHistory[target_id].append(self.greenOut)
                        elif not isUnder(feetPos[target_id], self.redParam) and isUnder(self.backupFeetPos[target_id], self.redParam):
                            events += [self.redIn] # ingresso rosso
                            self.eventsHistory[target_id].append(self.redIn)
                        elif isUnder(feetPos[target_id], self.redParam) and not isUnder(self.backupFeetPos[target_id], self.redParam):
                            events += [self.redOut] # uscita rosso
                            self.eventsHistory[target_id].append(self.redOut)
                        elif self.box.isInBox(feetPos[target_id]):
                            events += [100] # uscita rosso
                        else:
                            #events += [0]
                            events += [self.box.crossBoxBorders(target_id,feetPos[target_id])]
                            # TODO:così ancora non va bene: l'occorrenza di un evento diventa esclusiva, ma in base a come posiziono aree e linee potrebbero avvenire eventi in concomitanza
                    else:
                        events += [0]

                    # aggiorno le ultime posizioni regstrate per i piedi
                    self.backupFeetPos[target_id] = feetPos[target_id]

                    #if events[idx] != 0:
                        #self.logger.debug('{} - {} - {}'.format(eval(combinedDetection[0][1][0][1][b'detections'])[b'ref'],target_id,events[idx]))

                    #print(type(target_id))
                    if target_id in newPairs:
                        output_target = tm.target(target['screen_box'],target['screen_feet'],auth_code=newPairsAuth[target_id],tag_id=newPairs[target_id],is_badge=target['is_badge'],target_id=target['target_id'], zone_events=events)
                    else:
                        output_target = tm.target(target['screen_box'],target['screen_feet'], target_id=target['target_id'], zone_events=events)

                    #print(output_target.format_object())
                    output_targets += [output_target]

                maxLen = 50  # per evitare che la lunghezza di self.backupFeetPos cresca indefinitamente
                clearOrderedDict(self.backupFeetPos,maxLen)

                formatted_targets = [ target.format_object() for target in output_targets ]

                drawInfo = {
                    b'area_id' : detections[b'area_id'],
                    b'camera_id': detections[b'camera_id'],
                    b'events_ts': detections[b'events_ts'],
                    b'frame_id': detections[b'frame_id'],
                    b'targets': json.dumps(formatted_targets)
                }

                self.db.xadd(self.streamDraw,drawInfo,maxlen=1000) 
                #print(drawInfo)

                
                

        except Exception as e:
            self.logger.error('Exception in Pairing', exc_info=True)
            showExceptionInfo(e,'Pairing')
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            #exit()

    def pairInFrame(self, combinedDetection):
        ''' 
        permette di associare l'id della CV detection a quello
        della detection delle antenne
        '''

        #self.backupID = self.lastID        
        self.lastID = combinedDetection[0][1][0][0]

        detections = eval(combinedDetection[0][1][0][1][b'detections'])

        targets = json.loads(detections[b'targets'])

        detectionsNumber = len(targets)
        #detectionsIDs = eval(detections[b'IDs'])

        tags = eval(combinedDetection[0][1][0][1][b'tags'])
        tagsNumber = int(tags[b'tagN'])
        tagIDs = tags[b'tagID']
        tagAuth = tags[b'tagAuth']


        alreadyBadged = []
        detectionsIDs = []
        feetPos = np.zeros((detectionsNumber, 2), dtype="int")
        for idx,target in enumerate(targets):
            feetPos[idx] = target['screen_feet']*self.img_shape
            #print(target['target_id'])
            #exit()
            detectionsIDs += [target['target_id']]

            if self.connection.isBadge(target['target_id']):
                alreadyBadged += [idx]

                if target['target_id'] in self.people:
                    self.deregisterPerson(target['target_id'])
          
        tagPos = np.zeros((tagsNumber, 2), dtype="int")  
        for counter in range(tagsNumber):
            tagPos[counter] = (tags[b'tagPos'][2*counter], tags[b'tagPos'][2*counter+1])
        
        D = dist.cdist(feetPos,tagPos) # distances matrix             
        D[D > self.dMax] = self.upperBound + 1 # substitute large distances with upper bound
        D[alreadyBadged] = self.upperBound + 1
        
        rows, cols = linear_sum_assignment(D) # evaluate pairings
        indexes = np.stack((rows,cols),axis=1) # rearrange couples in (x,y) shape        
        
        delRows = []
        delCols = []

        for (x,y) in indexes:
            if D[x][y] == self.upperBound + 1:              
                
                delRows += [x]
                delCols += [y]
                
        rows = np.setdiff1d(rows, delRows)
        cols = np.setdiff1d(cols, delCols)

        detectionsIDs = np.array(detectionsIDs)[rows.astype(int)]
        tagIDs = np.array(tagIDs)[cols.astype(int)]
        tagAuth = np.array(tagAuth)[cols.astype(int)]

        newPairs = OrderedDict()
        newPairsAuth = OrderedDict()
        idx = 0
        for id, tag in zip(detectionsIDs,tagIDs):
            newPairs[id] = tag
            newPairsAuth[id] = tagAuth[idx]
            idx+=1
            
        return newPairs, newPairsAuth

    def registerPerson(self,id,tag,auth):
        self.people[id] = tag
        self.disappeared[id] = 0
        self.authCode[id] = auth

    def deregisterPerson(self,id):
        self.connection.updateAuthDict(id,'-1',False)
        del self.people[id]
        del self.disappeared[id]
        del self.authCode[id]
        if id in self.eventsHistory:
            del self.eventsHistory[id]

    def updateDict(self, newPairs, newAuth):

        newPairsID = list(newPairs.keys()) 
        newAuthID = list(newAuth.keys())
        dictID = list(self.people.keys())

        intersection = np.intersect1d(newPairsID,dictID)
        onlyNew = np.setdiff1d(newPairsID,dictID)
        onlyDict = np.setdiff1d(dictID,newPairsID)
        
        for id in intersection:
            if self.people[id] == newPairs[id]:
                self.disappeared[id] = 0
            else:
                self.disappeared[id] += 1
                if self.disappeared[id] > self.maxDisappeared:
                    self.deregisterPerson(id) 
        
        for id in onlyNew:
            alreadyPresent = False
            for _,tagDict in self.people.items():
                if tagDict == newPairs[id]:
                    alreadyPresent = True
                    break
            if not alreadyPresent:
                self.registerPerson(id,newPairs[id],newAuth[id])

        for id in onlyDict:
            self.disappeared[id] += 1
            if self.disappeared[id] > self.maxDisappeared:
                self.deregisterPerson(id)    
        

if __name__ == '__main__':

    initialParameters = loadConfiguration()

    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--url', help='Redis URL', type=str, default='redis://127.0.0.1:6379')
    args = parser.parse_args()

    print('\nConfiguration parameters:')
    for name, value in initialParameters.items():
        print('\t{0:.<40}{1}'.format(name,value))
    print()  

    if os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', False):
        url = urlparse(args.url)    
    else:
        url = urlparse('redis://' + initialParameters['DATABASE']['HOST'] + ':' + initialParameters['DATABASE']['PORT'])

    message = pairing(url)
    
    while True:
        
        message.__next__()