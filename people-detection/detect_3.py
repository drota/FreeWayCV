import argparse
import collections
import time
from pathlib import Path
import sys
from typing import Collection
import cv2
import torch
import torch.backends.cudnn as cudnn
from numpy import random
import json

from models.experimental import attempt_load
from utils.datasets import LoadStreams, LoadImages
from utils.general import check_img_size, check_requirements, check_imshow, non_max_suppression, apply_classifier, \
    scale_coords, xyxy2xywh, strip_optimizer, set_logging, increment_path
from utils.plots import plot_one_box
from utils.torch_utils import select_device, load_classifier, time_synchronized

#from detectfromredis import redisDB, LoadImages777
import numpy as np

from redisCommunication import redisDB, LoadImages777

from centroidTracker3 import Tracker
from funzioni import loadConfiguration, feet, badgeEvent
import target_management_rt as targ

from urllib.parse import urlparse

reportFileDetection = open("detect_2-debug.txt","a")

initialParameters = loadConfiguration()
feetPosition = feet()

def loadModel():

    weights = opt.weights 
    # Initialize
    set_logging()
    device = select_device(opt.device)
    half = device.type != 'cpu'  # half precision only supported on CUDA

    # Load model
    model = attempt_load(weights, map_location=device)  # load FP32 model
    stride = int(model.stride.max())  # model stride
    #imgsz = check_img_size(imgsz, s=stride)  # check img_size                   #metto in detect insieme a imgsz
    if half:
        model.half()  # to FP16
    
    return device,half, model, stride

#@profile
def detect(device,half, model, stride, imgsz, source, save_img=False):    

    #save_img = True
    dataset = LoadImages777(source, img_size=imgsz, stride=stride)        

    # Run inference
    if device.type != 'cpu':
        model(torch.zeros(1, 3, imgsz, imgsz).to(device).type_as(next(model.parameters())))  # run once
    #t0 = time.time()
    for path, img, im0s, _ in dataset:
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        # Inference
        t1 = time_synchronized()
        pred = model(img, augment=opt.augment)[0]

        # Apply NMS
        pred = non_max_suppression(pred, opt.conf_thres, opt.iou_thres, classes=opt.classes, agnostic=opt.agnostic_nms)
        t2 = time_synchronized()

        # Process detections
        detections = []
        feet = []
        people = 0
        targets = []
        for i, det in enumerate(pred):  # detections per image
            p, s, im0 = path, '', im0s #, getattr(dataset, 'frame', 0)
            
            for *xyxy, conf, cls in reversed(det): #coordinate, confidenza, classe

                x1 = xyxy[0].item()
                y1 = xyxy[1].item()
                x2 = xyxy[2].item()
                y2 = xyxy[3].item()

                detections += [x1,y1,x2,y2]
                feet_computation = feetPosition.position(x1,y1,x2,y2)
                feet += feet_computation[0]
                people += 1

                normalized_detection = [x1/initialParameters['FRAME']['FRAME_WIDTH'],y1/initialParameters['FRAME']['FRAME_HEIGHT'],x2/initialParameters['FRAME']['FRAME_WIDTH'],y2/initialParameters['FRAME']['FRAME_HEIGHT']]

                current_target = targ.target(normalized_detection,feet_computation[1])
                targets.append(current_target)

                
            if opt.view_img:                
                for *xyxy, conf, cls in reversed(det): #coordinate, confidenza, classe
                    label = f'{conf:.2f}'
                    plot_one_box(xyxy, im0, label=label, line_thickness=3,marker=True)
                cv2.imshow(str(p), im0)
                #cv2.waitKey(0)  # 1 millisecond           

           
    return detections, people, feet, targets


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', nargs='+', type=str, default=initialParameters['DETECTIONS']['WEIGHTS_FILE'], help='model.pt path(s)')
    parser.add_argument('--source', type=str, default='data/images', help='source')  # file/folder, 0 for webcam
    parser.add_argument('-a', '--area', help='Area identifying number', type=str, default=initialParameters['DATABASE']['AREA_NUMBER'])
    parser.add_argument('-c', '--camera', help='Camera identifying number', type=str, default=initialParameters['DATABASE']['CAMERA_NUMBER'])
    parser.add_argument('--img-size', type=int, default=640, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=initialParameters['DETECTIONS']['CONFIDENCE_TRESHOLD'], help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=initialParameters['DETECTIONS']['IOU_TRESHOLD'], help='IOU threshold for NMS')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--view-img', action='store_false', default=False, help='display results')
    parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')
    parser.add_argument('--save-conf', action='store_true', help='save confidences in --save-txt labels')
    parser.add_argument('--classes', nargs='+', type=int, help='filter by class: --class 0, or --class 0 2 3')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    parser.add_argument('--update', action='store_true', help='update all models')
    parser.add_argument('--project', default='runs/detect', help='save results to project/name')
    parser.add_argument('--name', default='exp', help='save results to project/name')
    parser.add_argument('--exist-ok', action='store_true', help='existing project/name ok, do not increment')
    opt = parser.parse_args()
    #print(opt)
    check_requirements()
    
    # Load configuration parameters
    print('\nConfiguration parameters:')
    for name, value in initialParameters.items():
        print('\t{0:.<20}{1}'.format(name,value))
    print()

    inputStream = 'area:' + opt.area + ':camera:' + opt.camera
    outputDetections =  inputStream + ':yolo'
    outputfeet =        inputStream + ':feet'
    streamBadge = 'e:areas:{}:badges:events'.format('10') # TODO: rimetto a 0
    authDict = 'area:{}:idVsAuth'.format(opt.area)

    # Load badge readers
    badgeReader = {}
    for reader in initialParameters['BADGE_READER'].items():
        badgeReader[reader[1]['ID']] = (reader[1]['X']*initialParameters['FRAME']['FRAME_WIDTH'], reader[1]['Y']*initialParameters['FRAME']['FRAME_HEIGHT'])

    # Redis connection
    db = redisDB(initialParameters['DATABASE']['HOST'],initialParameters['DATABASE']['PORT'],inputStream,outputDetections,outputfeet, streamBadge,authDict)
    
    # Set centroid tracker
    last_id = db.get_last_id()
    ct = Tracker(initialParameters['TRACKING']['TRACKING_TTL'],initialParameters['TRACKING']['LIMIT_DISTANCE'],initialParameters['TRACKING']['DEBUG_FILE'],initialParameters['FRAME']['FRAME_WIDTH'],initialParameters['FRAME']['FRAME_HEIGHT'],last_id) # oggetto per il tracking del centroide

    sum = 0
    counter = 0

    with torch.no_grad():
        if opt.update:  # update all models (to fix SourceChangeWarning)
            for opt.weights in ['yolov5s.pt', 'yolov5m.pt', 'yolov5l.pt', 'yolov5x.pt']:
                strip_optimizer(opt.weights)
        else:    

            data = loadModel()
            stride = data[3]
            streamID = b'0'
            streamID_old = b'0'
            while True:
                #time0 = time.time()
                #counter += 1
                try:
                    ret = False
                    while not ret or streamID == streamID_old:
                        streamID_old = streamID
                        ret, streamID, frame = db.getFrameFromRedis()
                        new = streamID

                    #TODO: aggiungere try/except per queste operazioni
                    buf = frame[b'image']
                    nparr = np.frombuffer(buf, np.uint8)
                    img = cv2.imdecode(nparr,cv2.IMREAD_UNCHANGED)    
                    
                    imgsz = img.shape[1]
                    imgsz = check_img_size(imgsz, s=stride)  # check img_size
                    
                    detections, people, feet, targets = detect(*data, imgsz, img)
                    
                    ct.update(targets) # eventually return targets

                    for idx, target in enumerate(targets):
                        authorization = db.getAuthDictID(target.target_id) 
                        #print('auth: {}'.format(authorization))
                        #print(target.target_id)
                        if authorization is None:
                            db.updateAuthDict(target.target_id, '-1', False)

                    # manage badge
                    badge_stream_id, badge_id, reader_id = db.getBadgeEvent()
                    if badge_stream_id:
                        
                        reader_position = badgeReader[int(reader_id)]
                        frame_shape = (initialParameters['FRAME']['FRAME_WIDTH'],initialParameters['FRAME']['FRAME_HEIGHT'])
                        badgingID, targets_idx = badgeEvent(reader_position,targets,frame_shape)
                        
                        if badgingID:

                            print('badged')

                            db.updateAuthDict(badgingID, '0', True)
                            targets[targets_idx].update_after_badge_event(badge_id)

            
                    # prepare data for stream
                    if people > 0:

                        #authDict = db.getAuthDict()
                        formatted_targets = [ target.format_object() for target in targets ]
                        
                        detection_message = {

                            "area_id" : opt.area,
                            "camera_id": opt.camera,
                            "events_ts": '{:.0f}'.format(time.time()*1e3), # al momento del salvataggio
                            "frame_id": streamID,
                            "targets": json.dumps(formatted_targets)

                        }

                        feet_message = {

                            "area_id" : opt.area,
                            "camera_id": opt.camera,
                            "events_ts": '{:.0f}'.format(time.time()*1e3), # al momento del salvataggio
                            "frame_id": str(streamID),
                            "targets": formatted_targets

                        }

                        #print()
                        #for target in json.loads(detection_message['targets']):
                        #    print(target)
                        #print()
                        
                        db.detections2RedisNew(detection_message, feet_message)
                        #print()

                    #time1 = time.time()
                    #sum += time1-time0 
                    #print('mean = {:.4f} s.'.format(sum/counter))

                except KeyboardInterrupt as k:
                    #TODO: integro con la funzione che gestisce le eccezioni
                    print(k)
                    break  
                except Exception as e:
                    #TODO: integro con la funzione che gestisce le eccezioni
                    print(e)
                    continue

reportFileDetection.close()  
ct.closeStream()
