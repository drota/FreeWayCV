import redis
from urllib.parse import urlparse
import json

class redis_manager():

    def __init__(self):

        shared_state = {}
        self.__dict__ = shared_state

    def connect(self, host, port):
        
        connectionPool = redis.ConnectionPool(host=host, port=port, db=0)

        self.connection = redis.StrictRedis(connection_pool=connectionPool)

        self.authDict = 'area:0:idVsAuth'
        
        if not self.connection.ping():
            raise Exception('Redis unavailable')
        else:
            print('Redis connection OK')
            return self.connection

        
    def updateAuthDict(self, id, auth, isBadge):
        '''
        Update the dictionary id -> (authorization Code, isBadge)

        Arguments:
            -   id: id of the badging person from the detection
            -   auth: authorization code
            -   isBadge: bool to check if the authorization come from badge
        '''
        data = {
            "auth":str(auth),
            "isBadge":isBadge
        }

        dictName = self.authDict 
        ttl = 1800 # seconds
        self.connection.hset(name=dictName,key=str(id),value=json.dumps(data))
        self.connection.expire(name=dictName, time=ttl)

    def isBadge(self,id):
        '''
        Return True if the id is paired to a badge event, False otherwise
        '''
        data = self.connection.hget(self.authDict,str(id))
        #print('isBadge: {}'.format(data))
        if data:
            return json.loads(data)['isBadge']
        else:
            #print('nessuna voce: {}'.format(id))
            return False

if __name__ == '__main__':
    host = '127.0.0.1'
    port = '6379'

    db = redis_manager().connect(host,port)

    print(db.isBadge(1000))
    #redisManager = redis_manager()