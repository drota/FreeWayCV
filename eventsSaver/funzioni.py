import yaml
import numpy as np
import cv2 as cv
import sys
import os

def loadConfiguration(path="./initialization.yaml"):
   
    with open(path, 'r') as config:
            try:
                data = yaml.safe_load(config)
                return data

            except yaml.YAMLError as exc:
                print(exc)
                exit

def showExceptionInfo(e, section):
        
    print('\033[91mEccezione\033[00m in {}: {}'.format(section,e))
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)

def isUnder(coordinates,line):
    (m,q) = line
    return (coordinates[1] < m*coordinates[0] + q)

def clearOrderedDict(dict, max_size):
    
    if len(dict) >= max_size:
        diff = len(dict) - max_size 
        for k in list(dict.keys())[:diff]:
            del dict[k]

def evaluateLineParameters(points):
    ''' return (m,q) for a straight line given two points'''

    x1 = points[0][0]
    y1 = points[0][1]
    x2 = points[1][0]
    y2 = points[1][1]
    
    m = (y1-y2)/(x1-x2)
    q = (x1*y2 - x2*y1)/(x1 - x2)

    return (m,q)

class camera():
    def __init__(self, initialParameters):
        
        try:
            
            objp = np.array(initialParameters['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,:3]
            
            image_width_input = 800 #initialParameters['FRAME']['FRAME_WIDTH']
            image_height_input = 600 #initialParameters['FRAME']['FRAME_HEIGHT']
            self.image_width_output = initialParameters['FRAME']['FRAME_WIDTH']
            self.image_height_output = initialParameters['FRAME']['FRAME_HEIGHT']

            px = np.array(initialParameters['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,3:5][:,0] / image_width_input
            py = np.array(initialParameters['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,3:5][:,1] / image_height_input
            pxpy = np.stack((px,py), axis=1)
            #pxpy = np.array(zip(px,py))
            #print(pxpy)

            # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0) # new version
            objpoints = [objp.astype('float32')]

            # image points
            pxpy = np.expand_dims(pxpy,axis=1)
            imgpoints = [pxpy.astype('float32')]

            # Calibration
            self.ret, self.mtx, self.dist, self.rvecs, self.tvecs = cv.calibrateCamera(objpoints, imgpoints, (1,1), None, None)

        except yaml.YAMLError as exc:
            print(exc)


    def computeWorldToPixel(self, x, y):
        # world coordinates -> pixel coordinates
        xyz = [x, y, 0]
        imgpoints2, _ = cv.projectPoints((np.array(xyz)).astype('float32'), self.rvecs[0], self.tvecs[0], self.mtx, self.dist)
        
        imgpoints2[:,:,0] *= self.image_width_output
        imgpoints2[:,:,1] *= self.image_height_output

        return imgpoints2[0,0]



            