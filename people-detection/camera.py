import yaml
import numpy as np
from numpy.linalg import inv
import cv2 as cv

class camera():
    def __init__(self, initialParameters):

        try:
            
            objp = np.array(initialParameters['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,:3]
            
            self.image_width_input = initialParameters['CAMERA_CALIBRATION']['IMAGE_SIZE_X_PIXEL']
            self.image_height_input = initialParameters['CAMERA_CALIBRATION']['IMAGE_SIZE_Y_PIXEL']
            self.image_width_output = initialParameters['FRAME']['FRAME_WIDTH']
            self.image_height_output = initialParameters['FRAME']['FRAME_HEIGHT']

            px = np.array(initialParameters['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,3:5][:,0]
            py = np.array(initialParameters['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,3:5][:,1]
            pxpy = np.stack((px,py), axis=1)

            # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0) # new version
            objpoints = [objp.astype('float32')]

            # image points
            pxpy = np.expand_dims(pxpy,axis=1)
            imgpoints = [pxpy.astype('float32')]

            # Calibration
            self.ret, self.mtx, self.dist, self.rvecs, self.tvecs = cv.calibrateCamera(objpoints, imgpoints, (self.image_width_input,self.image_height_input), None, None)
            self.rotation_matrix, _ = cv.Rodrigues(np.asarray(self.rvecs).reshape((3,1)))
            
        except yaml.YAMLError as exc:
            print(exc)

    def computeWorldToPixel(self, x, y):

        # world coordinates -> pixel coordinates
        xyz = [x, y, 0.]
        imgpoints2, _ = cv.projectPoints((np.array(xyz)).astype('float32'), self.rvecs[0], self.tvecs[0], self.mtx, self.dist)
        imgpoints2[:,:,0] *= (self.image_width_output / self.image_width_input)
        imgpoints2[:,:,1] *= (self.image_height_output / self.image_height_input)

        return imgpoints2[0,0]

    def compute_pixel_to_world(self, u, v):  

        camMat = np.asarray(self.mtx)
        rotMat = self.rotation_matrix
        tvec = self.tvecs[0]
        z = 0.0

        iRot = inv(rotMat)
        iCam = inv(camMat)

        uvPoint = np.ones((3, 1))

        # Image point
        uvPoint[0, 0] = u / self.image_width_output * self.image_width_input
        uvPoint[1, 0] = v / self.image_height_output * self.image_height_input

        tempMat = np.matmul(np.matmul(iRot, iCam), uvPoint)
        tempMat2 = np.matmul(iRot, tvec)

        s = (z + tempMat2[2, 0]) / tempMat[2, 0]
        wcPoint = np.matmul(iRot, (np.matmul(s * iCam, uvPoint) - tvec))

        # wcPoint[2] will not be exactly equal to z, but very close to it
        assert int(abs(wcPoint[2] - z) * (10 ** 8)) == 0
        wcPoint[2] = z

        return wcPoint[:2].reshape(2)

if __name__ == '__main__':
    pass