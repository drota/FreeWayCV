import redis
from urllib.parse import urlparse
import json

class redis_manager():

    def __init__(self,hostname='127.0.0.1',port='6379'):

        shared_state = {}
        self.__dict__ = shared_state

        self.authDict = 'area:0:idVsAuth'

        connectionPool = redis.ConnectionPool(host=hostname, port=port, db=0)
        self.connection = self._connect(connectionPool)

        self.max_dets_tags_delay = int(3e3) # ms

    def _connect(self,connectionPool):

        connection = redis.StrictRedis(connection_pool=connectionPool)
        
        if not connection.ping():
            raise Exception('Redis unavailable')
        else:
            print('Redis connection OK')
            return connection

        
    def updateAuthDict(self, id, auth, isBadge):
        '''
        Update the dictionary id -> (authorization Code, isBadge)

        Arguments:
            -   id: id of the badging person from the detection
            -   auth: authorization code
            -   isBadge: bool to check if the authorization come from badge
        '''
        data = {
            "auth":str(auth),
            "isBadge":isBadge
        }

        dictName = self.authDict 
        ttl = 1800 # seconds
        self.connection.hset(name=dictName,key=str(id),value=json.dumps(data))
        self.connection.expire(name=dictName, time=ttl)

    def isBadge(self,id):
        '''
        Return True if the id is paired to a badge event, False otherwise
        '''
        data = self.connection.hget(self.authDict,id)
        if data:
            return json.loads(data)['isBadge']
        else:
            return False

    def xread_detections(self,stream_name,t):
        return self.connection.xread({stream_name: t},count=1)    

    def xread_tags(self,stream_name,t):
        return self.connection.xread({stream_name: t},count=1)

    def xadd_detection_tag(self, stream_name, tagdet):
        self.connection.xadd(stream_name,tagdet,maxlen=1000)

    def xread_det_tag(self, stream_dets, t_dets, stream_tags):
        '''
        Read first detection after t_dets and xrevrange last tags snapshot if older than t_min.
        '''
        p = self.connection.pipeline()
        p.xread({stream_dets: t_dets},count=1) 
        t_min =  t_dets - self.max_dets_tags_delay
        p.xrevrange(stream_tags,max='+',min=t_min,count=1)  

        det, tag = p.execute()
        return det, tag

if __name__ == '__main__':
    host = '127.0.0.1'
    port = '6379'

    db = redis_manager(host,port)#._connect(host,port)
    print(db.updateAuthDict(3000,-1,False))
    print(db.isBadge(3000))
    #redisManager = redis_manager()