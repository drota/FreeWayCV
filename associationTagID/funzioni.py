import yaml
import numpy as np
from numpy.linalg import inv
import cv2 as cv
import sys
import os
import copy
from shapely.geometry import Point, Polygon

def loadConfiguration(path="./initialization.yaml"):
   
    with open(path, 'r') as config:
            try:
                data = yaml.safe_load(config)
                return data

            except yaml.YAMLError as exc:
                print(exc)
                exit

def showExceptionInfo(e, section):
        
    print('\033[91mEccezione\033[00m in {}: {}'.format(section,e))
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)

def isUnder(coordinates,line):
    (m,q) = line
    return (coordinates[1] < m*coordinates[0] + q)

def clearOrderedDict(dict, max_size):
    
    if len(dict) >= max_size:
        diff = len(dict) - max_size 
        for k in list(dict.keys())[:diff]:
            del dict[k]

def evaluateLineParameters(points):
    ''' return (m,q) for a straight line given two points'''

    x1 = points[0][0]
    y1 = points[0][1]
    x2 = points[1][0]
    y2 = points[1][1]
    
    m = (y1-y2)/(x1-x2)
    q = (x1*y2 - x2*y1)/(x1 - x2)

    return (m,q)

class camera():
    def __init__(self, initialParameters):

        try:
            
            objp = np.array(initialParameters['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,:3]
            
            self.image_width_input = initialParameters['CAMERA_CALIBRATION']['IMAGE_SIZE_X_PIXEL']
            self.image_height_input = initialParameters['CAMERA_CALIBRATION']['IMAGE_SIZE_Y_PIXEL']
            self.image_width_output = initialParameters['FRAME']['FRAME_WIDTH']
            self.image_height_output = initialParameters['FRAME']['FRAME_HEIGHT']

            px = np.array(initialParameters['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,3:5][:,0]
            py = np.array(initialParameters['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,3:5][:,1]
            pxpy = np.stack((px,py), axis=1)

            # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0) # new version
            objpoints = [objp.astype('float32')]

            # image points
            pxpy = np.expand_dims(pxpy,axis=1)
            imgpoints = [pxpy.astype('float32')]

            # Calibration
            self.ret, self.mtx, self.dist, self.rvecs, self.tvecs = cv.calibrateCamera(objpoints, imgpoints, (self.image_width_input,self.image_height_input), None, None)
            self.rotation_matrix, _ = cv.Rodrigues(np.asarray(self.rvecs).reshape((3,1)))
            
        except yaml.YAMLError as exc:
            print(exc)

    def computeWorldToPixel(self, x, y):

        # world coordinates -> pixel coordinates
        xyz = [x, y, 0.]
        imgpoints2, _ = cv.projectPoints((np.array(xyz)).astype('float32'), self.rvecs[0], self.tvecs[0], self.mtx, self.dist)
        imgpoints2[:,:,0] *= (self.image_width_output / self.image_width_input)
        imgpoints2[:,:,1] *= (self.image_height_output / self.image_height_input)

        return imgpoints2[0,0]

    def compute_pixel_to_world(self, u, v):  

        camMat = np.asarray(self.mtx)
        rotMat = self.rotation_matrix
        tvec = self.tvecs[0]
        z = 0.0

        iRot = inv(rotMat)
        iCam = inv(camMat)

        uvPoint = np.ones((3, 1))

        # Image point
        uvPoint[0, 0] = u / self.image_width_output * self.image_width_input
        uvPoint[1, 0] = v / self.image_height_output * self.image_height_input

        tempMat = np.matmul(np.matmul(iRot, iCam), uvPoint)
        tempMat2 = np.matmul(iRot, tvec)

        s = (z + tempMat2[2, 0]) / tempMat[2, 0]
        wcPoint = np.matmul(iRot, (np.matmul(s * iCam, uvPoint) - tvec))

        # wcPoint[2] will not be exactly equal to z, but very close to it
        assert int(abs(wcPoint[2] - z) * (10 ** 8)) == 0
        wcPoint[2] = z

        return wcPoint[:2].reshape(2)


class Line():

    '''
    Classe per la definizione di linee e dei metodi per valutare il loro attraversamento e in quale senso.

    Logica usata: vengono definite due zone per ogni linea: 
        - zona A: se la linea non è verticale è la zona al di sopra della linea stessa (sotto in termini di pixel); se la linea è verticale
        - zona B: non A

         A          /      \\         |
      -------    A / B    B \\ A    A | B
         B        /          \\       |
    '''
    def __init__(self, lineID, line_color, linePoints, a2b, b2a, area, camera ):

        '''
            Arguments:
                - lineID: Line ID
                - line_color: color of the line
                - a2b: event code when someone goes from A to B zone
                - b2a: event code when someone goes from B to A zone
                - area: Area ID
                - camera: Camera ID
        '''

        self.area = area
        self.camera = camera
        self.lineID = lineID
        self.color = line_color

        self.x1 = linePoints[0]
        self.y1 = linePoints[1]
        self.x2 = linePoints[2]
        self.y2 = linePoints[3]

        if self.x1 != self.x2:
            self.m = (self.y1-self.y2)/(self.x1-self.x2)
            self.q = (self.x1*self.y2 - self.x2*self.y1)/(self.x1 - self.x2)
        else:
            print('linee verticali non gestite: uscita')
            exit()

        self.zone_A_elements = set(()) # insieme degli elementi in zona A
        self.zone_B_elements = set(()) # insieme degli elementi in zona A

        self.cross_A_to_B    = a2b
        self.cross_B_to_A   = b2a
        self.eventNone  = 0

    def is_in_A(self, coordinates):
        if self.x1 != self.x2:
            # linea non verticale
            return (coordinates[1] < self.m*coordinates[0] + self.q)
        else:
            # linea verticale
            return (coordinates[0] < self.x1)

    def is_in_B(self, coordinates):
        return not self.is_in_A(coordinates)

    def crossLine(self, id, feetXY):     

        '''
            Return different values when:
                - Someone cross the line from zone A to zone B
                - Someone cross the line from zone B to zone A
                - No one cross the line

            Arguments:
                - id: detection ID
                - feetXY: [fx,fy] feet coordinates
            
            Returns:
                - Event code
        '''

        if self.is_in_A(feetXY) and id not in self.zone_A_elements:
            self.zone_A_elements.add(id)
            if id in self.zone_B_elements:
                self.zone_B_elements.remove(id)            
                return self.cross_B_to_A 
                
        elif self.is_in_B(feetXY) and id not in self.zone_B_elements:
            self.zone_B_elements.add(id)
            if id in self.zone_A_elements:
                self.zone_A_elements.remove(id)
                return self.cross_A_to_B 
                
        return self.eventNone

    def getIdUnderLine(self):
        return self.underLine

class Box():

    '''
    Classe per la definizione di aree e dei metodi per valutare la presenza e l'ingresso in essa
    '''
    def __init__(self, boxID, box_color, boxCoords, event_in, event_out, auth, is_in_code, area, camera ):

        '''
            Arguments:
                - boxCoords: (x1,y1,x2,y2) Coordinates of the upper-left and lower-down corner of the box.
                - area: Area ID
                - camera: Camera ID
                - boxID: Box ID
        '''

        self.area = area
        self.camera = camera
        self.boxID = boxID
        self.box_color = box_color
        self.auth_needed = auth

        self.x1 = boxCoords[0]
        self.y1 = boxCoords[1]
        self.x2 = boxCoords[2]
        self.y2 = boxCoords[3]

        self.idInBox = set(())

        self.eventIn    = event_in
        self.eventOut   = event_out
        self.is_in_code = is_in_code
        self.eventNone  = 0


    def isInBox(self, feetXY):

        '''
            return true if a person at coordinates (fx,fy) is inside the box 
            whose upper left corner is at coordinate (x1,y1) and lower right coordinates at (x2,y2),
            false otherwise

            Arguments:
                - feetXY: [fx,fy] feet coordinates

            Returns:
                - True if feetXY are in Box, False otherwise
            
        '''
        
        fx = feetXY[0]
        fy = feetXY[1]

        return (fx > self.x1 and fx < self.x2 and fy > self.y1 and fy < self.y2)

    def crossBoxBorders(self, id, feetXY, auth):     

        '''
            Return different values when:
                - Someone jump into a box
                - Someone exit from the box
                - Box borders are not crossed

            Arguments:
                - id: detection ID
                - feetXY: [fx,fy] feet coordinates
            
            Returns:
                - Event code
        '''
        #print('{}\t{}'.format(auth,type(auth)))
        if self.isInBox(feetXY):
            if id not in self.idInBox:
                self.idInBox.add(id)
                return self.eventIn # ingresso
            elif auth != self.auth_needed:
                return self.is_in_code
        elif not self.isInBox(feetXY) and id in self.idInBox:
            self.idInBox.remove(id)
            return self.eventOut # uscita
            
        return self.eventNone

    def getIdInBox(self):
        return self.idInBox

    def check_id_in_box(self,id):
        return id in self.idInBox

class Area():

    def __init__(self, areaID, area_color, areaCoords, event_in, event_out, auth, is_in_code, area, camera ):

        '''
            Arguments:
                - areaCoords: [(x1,y1), (x2,y2), ...]: vertices of the polygon.
                - area: Area ID
                - camera: Camera ID
                - boxID: Box ID
        '''
        self.polygon = Polygon(areaCoords)

        self.area = area
        self.camera = camera
        self.areaID = areaID
        self.area_color = area_color
        self.auth_needed = auth

        # List of ID present in the current area.
        self.idInArea = set(())

        self.eventIn    = event_in
        self.eventOut   = event_out
        self.is_in_code = is_in_code
        self.eventNone  = 0

    def crossAreaBorders(self, id, feetXY, auth):     

        '''
            Return different values when:
                - Someone jump into an area
                - Someone exit from the area
                - Area borders are not crossed

            Arguments:
                - id: detection ID
                - feetXY: [fx,fy] feet coordinates
                - auth: authorization requested for the current area.
            
            Returns:
                - Event code
        '''

        feet_position = Point(feetXY[0],feetXY[1])
        if feet_position.within(self.polygon):
            
            if id not in self.idInArea:
                self.idInArea.add(id)
                return self.eventIn # ingresso
            elif auth != self.auth_needed:
                return self.is_in_code
        elif not feet_position.within(self.polygon) and id in self.idInArea:
            self.idInArea.remove(id)
            return self.eventOut # uscita
            
        return self.eventNone

    def getIdInArea(self):
        return self.idInArea

    def check_id_in_area(self,id):
        return id in self.idInArea


def compute_pairs(d,already_badged, max_value):

    D = copy.deepcopy(d)     
    feet_number, tag_number = D.shape

    used_rows = already_badged
    used_cols = list(np.where(np.all(D == max_value, axis=0))[0])
    pairs = []
    while True:

        if np.all(max_value == D):
            break
        if feet_number > tag_number and len(used_cols) == tag_number:
            break
        elif feet_number < tag_number and  len(used_rows) == feet_number:
            break
        elif len(used_rows) == feet_number and len(used_cols) == tag_number:
            break        
        
        
        rows = D.min(axis=1).argsort()
        cols = D.argmin(axis=1)[rows]

        for (x,y) in zip(rows,cols):
            
            if x in used_rows:
                if y not in used_cols:
                    used_cols += [y]
                break
            elif y in used_cols:
                if x not in used_rows:
                    used_rows += [x]
                break
            else:
                D[x] = max_value
                D[:,y] = max_value
                used_rows += [x]
                used_cols += [y]
                pairs += [(x,y)]     
          
        #print('#####\tstart {}\t######'.format(counter))
        #print('{}\n{}\nfeet number: {}\nused rows: {}\ntag number: {}\nused cols: {}'.format(D, pairs, feet_number,len(used_rows), tag_number, len(used_cols)))
        #print(np.all(D == max_value))
        #print('#####\tend {}\t######'.format(counter))

    pairs = np.array(pairs)
    #if np.all(D == max_value):
    #    print(D)
    #    print(pairs)
    return pairs

if __name__ == '__main__':
    
    param = loadConfiguration()
    cam = camera(param)
    cam_1 = camera(param, 1)
    cam_2 = camera(param, 2)

    objp = np.array(param['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,:3]

    px = np.array(param['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,3:5][:,0]
    py = np.array(param['CAMERA_CALIBRATION']['CALIB'], dtype='f')[:,3:5][:,1]
    pxpy = np.stack((px,py), axis=1)


    #print(cam.mtx)
    for i in range(len(objp)):
        print()
        print('World Coordinates: ({:.2f}, {:.2f})'.format(objp[i,0],objp[i,1]))
        print('Pixel Coordinates expected: {}'.format(pxpy[i]))
        print('Pixel Coordinates obtained 0: {}'.format(cam.computeWorldToPixel(*objp[i,:2])))
        print('Pixel Coordinates obtained 1: {}'.format(cam_1.computeWorldToPixel(*objp[i,:2])))
        print('Pixel Coordinates obtained 2: {}'.format(cam_2.computeWorldToPixel(*objp[i,:2])))
        print('Inverse:')
        print('World Coordinates obtained 0: ({:.2f}, {:.2f})'.format(*cam.compute_pixel_to_world(*pxpy[i])))
        print('World Coordinates obtained 1: ({:.2f}, {:.2f})'.format(*cam_1.compute_pixel_to_world(*pxpy[i])))
        print('World Coordinates obtained 2: ({:.2f}, {:.2f})'.format(*cam_2.compute_pixel_to_world(*pxpy[i])))
   
        sum_sq = np.sum(np.square(objp[i,:2] - cam_2.compute_pixel_to_world(*pxpy[i])))

        print('Distance: {:.2f} m'.format(np.sqrt(sum_sq)))

    
    u, v = 697. / 1200. * 800., 414. / 900. * 600.
    print('({}, {}) --> ({:.2f}, {:.2f})'.format(u, v, *cam_2.compute_pixel_to_world(u,v)))
    # TODO: calcolare camera matrix con la scacchiera
    # TODO: calcolare rvec e tvec con solvePnp