import redis
from urllib.parse import urlparse

class redis_manager():

    def __init__(self):

        shared_state = {}
        self.__dict__ = shared_state

    def connect(self, host, port):
        
        connectionPool = redis.ConnectionPool(host=host, port=port, db=0)

        connection = redis.StrictRedis(connection_pool=connectionPool)
        
        if not connection.ping():
            raise Exception('Redis unavailable')
        else:
            #print('Redis connection OK')
            return connection


if __name__ == '__main__':
    host = '127.0.0.1'
    port = '6379'

    db = redis_manager().connect(host,port)
    pass
    #redisManager = redis_manager()