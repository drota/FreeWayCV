import json

class target():
    def __init__(self, tag_id, tag_pos, tag_auth) -> None:
        '''
            Class where information about a detection target are stored

            Parameters:

                - tagID: id del tag
                - tagPos: posizione del tag (normalizzata)
                - tagAuth: autorizzazione associata al tag
        '''

        self.tag_id = tag_id
        self.screen_feet = screen_feet
        self.auth_code = str(auth_code)
        self.tag_id = tag_id
        self.is_badge = is_badge
        self.target_id = target_id
        self.zone_events = zone_events
        self.tag_ground_position = tag_ground_position


    def update_after_tracking(self, target_id):
        ''' 
        Update target ID after tracking.

        Arguments:
            - target_id: detection ID after tracking
        
        Returns:
            - None
        '''

        self.target_id = str(target_id)

    def update_after_badge_event(self,tag_id,auth_code='0'):
        ''' 
        Update information after a badge event for the target.

        Arguments:
            - tag_id: ID from badge or beacon
            - auth_code: authorization code
        
        Returns:
            - None
        '''

        self.auth_code = str(auth_code),
        self.tag_id = tag_id,
        self.is_badge = True

    def update_target_id(self, new_id):
        '''
        Update target_id

        Arguments:
            - new_id: updated target id
        '''
        self.target_id = str(new_id)

    def format_object(self, serialized=False):

        formatted_target = {
            
            'auth_code': self.auth_code,
            'tag_id': self.tag_id,
            'is_badge': self.is_badge,
            'ground_position': self.tag_ground_position, # TODO: funzione inversa pixel -> world
            'screen_box': self.screen_box, # coordinate del rettangolo
            'screen_feet': self.screen_feet ,
            'target_id': self.target_id,
            'target_type': 'PERSON',
            'zone_events': json.dumps(self.zone_events)
                    
        }

        if serialized:
            formatted_target = json.dumps(formatted_target)

        return formatted_target


if __name__ == '__main__':
    targ = target([0,0,0,0],[0,0])

    print(targ.format_object())
    print(targ.format_object(serialized=True))

    x = {
        't': [],
        'a': targ.format_object(serialized=True)
    }

    print(json.dumps(x))