# RedisEdge realtime video analytics web server
import argparse
import cv2
import io
import numpy as np
import redis
from urllib.parse import urlparse
from PIL import Image, ImageDraw, ImageFont
from flask import Flask, render_template, Response
import funzioni
from funzioni import loadConfiguration
import logging
import json

initialParameters = loadConfiguration()

class RedisImageStream(object):
    def __init__(self, conn, args):
        self.conn = conn
        self.camera = 'area:' + initialParameters['DATABASE']['AREA_NUMBER'] + ':camera:' + initialParameters['DATABASE']['CAMERA_NUMBER']
        self.boxes = self.camera + ':yolo'
        self.field = args.field.encode('utf-8') 

        self.frame_width = initialParameters['FRAME']['FRAME_WIDTH']
        self.frame_height = initialParameters['FRAME']['FRAME_HEIGHT']
        #self.lastDetID = self.conn.xrevrange(self.feet, count=1)[0][0]#'$'
        self.oldBmsg = -1
        self.oldCmsg = -1

    def get_last(self):
        ''' Gets latest from camera and model '''
        #p = self.conn.pipeline()
        #p.xrevrange(self.camera, count=1)  # Latest frame
        #p.xrevrange(self.boxes, count=1)   # Latest boxes
        #p.xrevrange(self.feet, count=1)   # Latest feet
        #cmsg, bmsg, fmsg = p.execute()
        cmsg = self.conn.xrevrange(self.camera, count=1)

        if cmsg:
            self.oldCmsg = cmsg
            last_id = cmsg[0][0].decode('utf-8')
            label = f'{self.camera}:{last_id}'
            data = io.BytesIO(cmsg[0][1][self.field])
            img = Image.open(data)
            draw = ImageDraw.Draw(img)
            #self.lastDetID = cmsg[0][0]
            #bmsg = self.conn.xread({self.boxes:last_id}, count=1,block=15)
            bmsg = self.conn.xread({self.boxes:last_id}, count=1,block=15) # TODO: forse meglio xrevrange
            if bmsg:
                #print(bmsg[0][1][0][1])
                targets = json.loads(bmsg[0][1][0][1][b'targets'])
                self.oldBmsg = bmsg

                for target in targets:

                    x1 = target['screen_box'][0] * self.frame_width
                    y1 = target['screen_box'][1] * self.frame_height
                    x2 = target['screen_box'][2] * self.frame_width
                    y2 = target['screen_box'][3] * self.frame_height

                    draw.rectangle(((x1, y1), (x2, y2)), width=1, outline='cyan')

                    # Centroid
                    cX, cY = funzioni.centroid(x1,y1,x2,y2)
                    r = 3
                    leftUpPoint = (cX-r, cY-r)
                    rightDownPoint = (cX+r, cY+r)
                    twoPointList = [leftUpPoint, rightDownPoint]
                    draw.ellipse(twoPointList, fill='cyan', outline='black')
                    
                    # feet  
                    #if fmsg:
                    feet = target['screen_feet']
                    feetCoordinatesX = float(feet[0]) * self.frame_width
                    feetCoordinatesY = float(feet[1]) * self.frame_height
                                        
                    r = 2
                    leftUpPoint = (feetCoordinatesX-r, feetCoordinatesY-r)
                    rightDownPoint = (feetCoordinatesX+r, feetCoordinatesY+r)
                    twoPointList = [ leftUpPoint, rightDownPoint ]
                    #print(twoPointList)
                    draw.ellipse(twoPointList, fill='yellow', outline='black')

                    # ID
                    fontSize = 15       
                    fontPath = './font/FreeMonoBold.ttf'
                    self.idFont = ImageFont.truetype(fontPath, fontSize)
                    idString = 'ID:{0}'.format(target['target_id'])
                    text_size = self.idFont.getsize(idString)
                    draw.rectangle(((x1+5, y1 -text_size[1]/2),(x1+5+text_size[0],y1  +text_size[1]/2)), width=1, outline=1,fill='cyan') # rettangolo id
                    draw.text((x1 + 5, y1 -fontSize/2), idString, font=self.idFont, fill='black',outline='black') # box id
                    #draw.text((x1 + 5, y1 + 5), str(target['target_id']), font=ImageFont.truetype(font,fontSize), fill=(255,0,0,255))

            arr = np.array(img)
            arr = cv2.cvtColor(arr, cv2.COLOR_BGR2RGB)
            cv2.putText(arr, label, (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,255), 1, cv2.LINE_AA)
            ret, img = cv2.imencode('.jpg', arr)
            return img.tobytes()
        else:
            # TODO: put an 'we're experiencing technical difficlties' image
            pass

def gen(stream):
    while True:
        frame = stream.get_last()
        yield (b'--frame\r\n'
               b'Pragma-directive: no-cache\r\n'
               b'Cache-directive: no-cache\r\n'
               b'Cache-control: no-cache\r\n'
               b'Pragma: no-cache\r\n'
               b'Expires: 0\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

conn = None
args = None
app = Flask(__name__)

@app.route('/video')
def video_feed():
    return Response(gen(RedisImageStream(conn, args)),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('camera', help='Input camera stream key', nargs='?', type=str, default='camera:0')
    parser.add_argument('boxes', help='Input model stream key', nargs='?', type=str, default='camera:0:yolo')
    parser.add_argument('--field', help='Image field name', type=str, default='image')
    parser.add_argument('--fmt', help='Frame storage format', type=str, default='.jpg')
    parser.add_argument('-u', '--url', help='Redis URL', type=str, default='redis://127.0.0.1:6379')
    args = parser.parse_args()

    # Set up Redis connection
    url = urlparse(args.url)
    conn = redis.Redis(host=url.hostname, port=url.port)
    if not conn.ping():
        raise Exception('Redis unavailable')
    
    app.run(host='0.0.0.0',port=5001)
