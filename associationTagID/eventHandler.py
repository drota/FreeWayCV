from redisManager import redis_manager

class eventHandler:

    def __init__(self, streamOutput, host, port, camera) -> None:
        '''
        event handler class
        '''

        self.streamOutput = streamOutput
        self.camera = camera
        self.db = redis_manager().connect(host,port)

    def registerEvent(self, eventCode, eventTS, tag=None, auth=None):
        
        event = {
            'camera': self.camera,
            'event_code': eventCode,
            'event_ts': eventTS,
            'tag': str(tag),
            'authorization_code': str(auth)
        }
        
        self.db.xadd(self.streamOutput,event,maxlen=1000)
        

if __name__ == '__main__':

    streamOutput = 'event:Queue'
    host = '127.0.0.1'
    port = '6379'
    camera = '101'

    eh = eventHandler(streamOutput,host,port, camera)
    eh.registerEvent(0,0)