import numpy as np

def camera_parameters():
    # Import parameters for feet detection (camera-calibration result)
    with np.load('mtx_p2p.npz') as X:
        mtx, dist, rvecs, tvecs = [X[i] for i in ('mtx','dist','rvecs','tvecs')]
    return mtx, dist,rvecs,tvecs