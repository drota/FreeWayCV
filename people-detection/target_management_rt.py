import json

class target():
    def __init__(self, screen_box, screen_feet, ground_position, auth_code='-1',tag_id='null', is_badge=False, target_id='', zone_events=[]) -> None:
        '''
            Class where information about a detection target are stored

            Parameters:

                - screen_box: detection box coordinates (normalized)
                - screen_feet: feet position (normalized)
                - auth_code: authorization code
                - tag_id: ID from badge or beacon
                - is_badge: boolean, True if the current target badged, False otherwise
                - target_id: detection ID after tracking
                - zone_events: list of the events occurred in the current frame for the current target
        '''

        self.screen_box = screen_box
        self.screen_feet = screen_feet
        self.ground_position = ground_position
        self.auth_code = auth_code
        self.tag_id = tag_id
        self.is_badge = is_badge
        self.target_id = target_id
        self.zone_events = zone_events


    def update_after_tracking(self, target_id, auth_code):
        ''' 
        Update target ID after tracking.

        Arguments:
            - target_id: detection ID after tracking
            - auth_code: authorization code
        
        Returns:
            - None
        '''

        self.target_id = str(target_id)
        self.auth_code = auth_code

    def update_after_badge_event(self,tag_id,auth_code='0'):
        ''' 
        Update information after a badge event for the target.

        Arguments:
            - tag_id: ID from badge or beacon
            - auth_code: authorization code
        
        Returns:
            - None
        '''

        self.auth_code = str(auth_code),
        self.tag_id = tag_id,
        self.is_badge = True

    def update_target_id(self, new_id):
        '''
        Update target_id

        Arguments:
            - new_id: updated target id
        '''
        self.target_id = str(new_id)

    def format_object(self, serialized=False):
        tag_id = None
        badge_id = None
        if self.is_badge:
            badge_id = self.tag_id
        else:
            tag_id = self.tag_id


        formatted_target = {
            
            'auth_code': int(self.auth_code),
            'tag_id': tag_id,
            'badge_id': badge_id,
            'ground_position': [ round(val,3) for val in self.ground_position ] , 
            'screen_box': [ round(val,3) for val in self.screen_box ], # coordinate del rettangolo
            'screen_feet': [ round(val,3) for val in self.screen_feet ]  ,
            'target_id': self.target_id,
            'target_type': 'PERSON',
            'zone_events': json.dumps(self.zone_events)
                    
        }

        if serialized:
            formatted_target = json.dumps(formatted_target)

        return formatted_target


if __name__ == '__main__':
    targ = target([0,0,0,0],[0,0])

    print(targ.format_object())
    print(targ.format_object(serialized=True))

    x = {
        't': [],
        'a': targ.format_object(serialized=True)
    }

    print(json.dumps(x))