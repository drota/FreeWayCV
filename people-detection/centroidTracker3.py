# https://www.pyimagesearch.com/2018/07/23/simple-object-tracking-with-opencv/
from math import sqrt
from scipy.spatial import distance as dist
from collections import OrderedDict
import numpy as np
import datetime
import os
import sys
from collections import Counter

class Tracker():
	def __init__(self, maxDisappeared, limitDistance, reportFileTracking, framewidth, frameheight,last_id):
		
		last_id = int(last_id)
		if last_id >= 99999:
			self.nextObjectID = 0
		else:
			self.nextObjectID = last_id + 1

		#print(last_id)

		#print('last_id: {}\nself.nextObjectID: {}'.format(last_id, self.nextObjectID))

		self.objects = OrderedDict()
		self.disappeared = OrderedDict()
		self.maxDisappeared = maxDisappeared

		self.results = dict()

		self.frame_width = framewidth
		self.frame_height= frameheight
		self.dMax = sqrt(pow(framewidth,2) + pow(frameheight,2)) # distanza massima possibile nel frame
		self.limitDistance = limitDistance # distanza limite otlre la quale registra un nuovo oggetto

		self.reportFileTracking = open(reportFileTracking,"a")

	def closeStream(self):
		self.reportFileTracking.close()

	def register(self, centroid):
		current_id = self.nextObjectID
		self.objects[current_id] = centroid
		self.disappeared[current_id] = 0
		if self.nextObjectID < 99999:
			self.nextObjectID += 1
		else:
			self.nextObjectID = 0

		#print(current_id)
		return current_id
			

	def deregister(self, objectID):
		
		del self.objects[objectID]
		del self.disappeared[objectID]

	def checkDeregisterStatus(self, objectID) :
		"""
		Incrementa lo stato di disappeared di un oggetto e verifica se questo dev'essere rimosso

		:param objectID: identificativo dell'oggetto da rimuovere
		"""
		self.disappeared[objectID] += 1
				
		if self.disappeared[objectID] > self.maxDisappeared:
			self.deregister(objectID)

	def checkInputOutputCoherence(self,people):
		''' controlla la coerenza tra input e output '''
		oggettiPresenti = sum( [ 1 for _ , x in self.objects.items() if x[2] != -999 ] )
		if oggettiPresenti - people == 0:
			return True
		else:
			return False

	def update(self, targets):

		people = len(targets)		

		try:

			# rappresenta l'id associato alla detection

			for _, det in self.objects.items():
				det[2] = -999

			if len(targets) == 0:
				
				for objectID in list(self.disappeared.keys()):
					self.checkDeregisterStatus(objectID)
						
				return False
				
			inputCentroids = np.zeros((people, 3), dtype="int")

			for idx, target in enumerate(targets):	

				xyxy = target.screen_box
				#print('xyxy = {}'.format(xyxy))

				cX = int((xyxy[0] + xyxy[2]) / 2.0 * self.frame_width)
				cY = int((xyxy[1] + xyxy[3]) / 2.0 * self.frame_height)
				inputCentroids[idx] = (cX, cY, idx)
				#print(inputCentroids[idx])


			if len(self.objects) == 0:
				#print('len == 0')
				for idx, target in enumerate(targets):
					id = self.register(inputCentroids[idx])
					target.update_target_id(id) 
			else:
				
				objectIDs = list(self.objects.keys())
				#print('objectIDs: {}'.format(objectIDs))
				objectCentroids = list(self.objects.values())

				# matrice (ij) delle distanze tra i-esimo oggetto e j-esimo input
				obj = np.array([(i[0],i[1]) for i in objectCentroids])
				inp =[(i[0],i[1]) for i in inputCentroids]

				D = dist.cdist(obj, inp)
				rows = D.min(axis=1).argsort()
				cols = D.argmin(axis=1)[rows]

				'''inserisco un controllo sui doppioni'''
				duplicate_values = [item for item, count in Counter(cols).items() if count > 1]
				for element in duplicate_values:

					duplicate_idxs = np.where(cols==element)[0][1:]
					rows_duplicate = rows[duplicate_idxs]
					rows = np.delete(rows,duplicate_idxs)
					cols = np.delete(cols,duplicate_idxs)

					for row in rows_duplicate:
						for col in D[row].argsort()[1:]:
							if col in cols:
								continue
							else:
								rows = np.append(rows,row)
								cols = np.append(cols,col)
								break
						else: # exit also from the loop over the rows
							continue
						break
				

				usedRows = set()
				usedCols = set()

				for (row, col) in zip(rows, cols):
					
					if row in usedRows or col in usedCols or D[row][col] == self.dMax:
						continue
					
					objectID = objectIDs[row]

					if D[row][col] <= self.limitDistance:
						self.objects[objectID] = inputCentroids[col]
						self.disappeared[objectID] = 0
					else:
						#objectID = objectIDs[row]
						self.checkDeregisterStatus(objectID)

						self.register(inputCentroids[col])

					target_idx = inputCentroids[col][2]
					targets[target_idx].update_target_id(objectID)
					
					usedRows.add(row)
					usedCols.add(col)

				unusedRows = set(range(0, D.shape[0])).difference(usedRows)
				unusedCols = set(range(0, D.shape[1])).difference(usedCols)

				if D.shape[0] >= D.shape[1]:
					# loop over the unused row indexes
					for row in unusedRows:
						
						objectID = objectIDs[row]
						self.checkDeregisterStatus(objectID)

				else:
					for col in unusedCols:
						self.register(inputCentroids[col])

						target_idx = inputCentroids[col][2]
						targets[target_idx].update_target_id(objectID)

				#for idx, target in enumerate(targets):
					###
				#	target.update_target_id(objectIDs[idx])

			if not self.checkInputOutputCoherence(people):
				raise ValueError()
			# return the set of trackable objects

			return targets

		except ValueError:
			print('\033[91mEccezione\033[00m in Tracking: output e input hanno dimensione diversa')

			exc_type, exc_obj, exc_tb = sys.exc_info()
			fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
			print(exc_type, fname, exc_tb.tb_lineno)

		except Exception as e:
			print('\033[91mEccezione\033[00m in Tracking: {}'.format(e))

			exc_type, exc_obj, exc_tb = sys.exc_info()
			fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
			print(exc_type, fname, exc_tb.tb_lineno)
	