import redis
from time import time
import time as t
from funzioni import camera
import sys
import os
import argparse
from urllib.parse import urlparse
from funzioni import loadConfiguration
import redisManager_new as RM
import logging
import logging.config
import yaml

class msg:
    """Redis message reader"""

    def __init__(self, url):

        self.db = RM.redis_manager(url.hostname, url.port)

        prefix = 'area:' + initialParameters['DATABASE']['AREA_NUMBER'] + ':camera:' + initialParameters['DATABASE']['CAMERA_NUMBER']
        self.streamDetections = prefix + ':yolo'
        self.streamBeacons = initialParameters['DATABASE']['STREAM_BEACON']
        self.streamTagDetections = prefix + ':tagDet'

        self.reception_ts = '$'
        
        self.TAG_TRANSMISSION_DELAY_MSEC = initialParameters['TIME_SYNC']['TAG_TRANSMISSION_DELAY_MSEC'] # Delta temporale con cui vado indietro a cercare nelle detections
        self.RITARDO_AGGIUNTIVO = initialParameters['TIME_SYNC']['RITARDO_AGGIUNTIVO'] # ms
        self.MAX_TIME_DISTANCE = initialParameters['TIME_SYNC']['MAX_TIME_DISTANCE'] # ms

        self.f = open(initialParameters['TIME_SYNC']['DELAYS_FILE'], 'a')

        self.frame_ts = 0
        self.tags = []

        self.camera = camera(initialParameters)

        with open('./pair_log_config.yaml', 'r') as f:
            config = yaml.safe_load(f.read())
            logging.config.dictConfig(config)
        self.logger = logging.getLogger('coupling_logger_debug')
        self.logger.setLevel('DEBUG')
        self.log_accepted_rateo = logging.getLogger('accepted_rejected')
        self.log_accepted_rateo.setLevel('DEBUG')


        
    def __iter__(self):
        return self

    def __next__(self):

        t = int(time()*1e3) - self.TAG_TRANSMISSION_DELAY_MSEC
        previousFrame_ts = self.frame_ts
        #det = self.db.xread({self.streamDetections: t},count=1, block=1000)
        det = self.db.xread_detections(self.streamDetections,t)

        if det:
            try:
                self.frame_ts = det[0][1][0][1][b'ref']

                if self.frame_ts != previousFrame_ts:
                    
                    t_det = int(self.frame_ts[:-2])
                    tags = self.db.xread_tags(self.streamBeacons,self.reception_ts)

                    #streamTagId = str(False)
                    if tags:
                        #streamTagId = tags[0][0]
                        self.reception_ts = int(tags[0][1][0][0][:-2])
                        self.tags = eval(tags[0][1][0][1][b'data'])
                        self.response_ts = self.tags[0]['response_ts']
                                            
                    tagID = []
                    tagPos = []
                    tagAuth = []

                    if self.reception_ts != '$':
                        
                        delay_tags_snapshot = self.reception_ts - time()*1e3

                        # se ho ricevuto un tag meno di 3 secondi fa
                        if abs(delay_tags_snapshot) < 3000:

                            tags_number = len(self.tags)

                            accepted = 0
                            rejected = 0

                            for tag in self.tags:
                                tag_id = tag['tag_id']
                                #tag_name = tag['tag_name']
                                position_ts = tag['position_ts']
                                pos = self.camera.computeWorldToPixel(tag['pos_x'],tag['pos_y'])
                                auth_code = tag['auth_code']

                                t_tag = int(self.reception_ts) - (self.response_ts - position_ts) - self.RITARDO_AGGIUNTIVO

                                info_ritardo = '{}\t{}\t{}'.format(self.TAG_TRANSMISSION_DELAY_MSEC,tags_number,t_det-t_tag)
                                self.logger.debug(info_ritardo)

                                if abs(t_tag - t_det) < self.MAX_TIME_DISTANCE:
                                    #if abs(t_tag - t_det) < self.MAX_TIME_DISTANCE*3:
                                    #    self.f.write('{}\n'.format(t_tag - t_det))
                                    #    self.f.flush()
                                    accepted += 1
                                    
                                    tagID += [tag_id]
                                    tagPos += [pos[0], pos[1]]
                                    tagAuth += [auth_code]
                                else:
                                    rejected += 1

                            #self.log_accepted_rateo.debug('{}\t{}'.format(accepted,rejected))

                    else:
                        self.reception_ts = int(time()*1e3)

                    print('############')
                    print('# \033[1m\033[96m{} tag(s)\033[0m #\n# \033[1m\033[92m{} people\033[0m #'.format(len(tagID),int(det[0][1][0][1][b'people'])))
                    print('############', end='\x1B[3A\r')

                    tagDet = {
                        b'detections' : str(det[0][1][0][1]),
                        b'tags' : str({

                            #b'streamTagID': streamTagId,
                            b'tagN' : len(tagID),
                            b'tagID' : tagID,
                            b'tagPos' : tagPos,
                            b'tagAuth' : tagAuth

                        })
                    }

                    self.db.xadd_detection_tag(self.streamTagDetections,tagDet)

            except Exception as e:
                print('\033[1m\033[91mEccezione\033[00m in msg: {}'.format(e))

                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
            
        else:
            people = 0
            try:
                tags = len(eval(self.db.xread_tags(self.streamBeacons,self.reception_ts)[0][1][0][1][b'data']))
            except:
                tags = 0
            print('############')
            print('# \033[1m\033[96m{} tag(s)\033[0m #\n# \033[1m\033[92m{} people\033[0m #'.format(tags,people))
            print('############', end='\x1B[3A\r')



if __name__ == '__main__':
    initialParameters = loadConfiguration()
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--url', help='Redis URL', type=str, default='redis://127.0.0.1:6379')
    args = parser.parse_args()

    if os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', False):
        url = urlparse(args.url)    
        #url = urlparse(initialParameters['DBURL'])
    else:
        url = urlparse('redis://' + initialParameters['DATABASE']['HOST'] + ':' + initialParameters['DATABASE']['PORT'])

    print('\nConfiguration parameters:')
    for name, value in initialParameters.items():
        print('\t{0:.<40}{1}'.format(name,value))
    print()  

    message = msg(url)


    while True:
        message.__next__()